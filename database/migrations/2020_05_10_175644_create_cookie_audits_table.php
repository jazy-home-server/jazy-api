<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCookieAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_cookie_audit_actions', function (Blueprint $table) {
            $table->id();
            $table->string('route')->unique();
        });
        Schema::create('meta_cookie_audits', function (Blueprint $table) {
            $table->id();
            $table->foreignId('action_id')->references('id')->on('meta_cookie_audit_actions');
            $table->string('origin')->nullable();
            $table->json('cookies')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_cookie_audits');
        Schema::dropIfExists('meta_cookie_audit_actions');
    }
}
