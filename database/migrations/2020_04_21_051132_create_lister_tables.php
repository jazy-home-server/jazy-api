<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListerTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Please use plural resource names for table names in the future
        Schema::create('lister_ac_list', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('pin');
            $table->timestamps();
        });
        Schema::create('lister_ac_list_song', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('list_id');
            $table->unsignedBigInteger('song_id');
            $table->boolean('checked');
            $table->timestamps();
        });
        Schema::create('lister_ac_song', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('img_url');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lister_ac_list');
        Schema::dropIfExists('lister_ac_list_song');
        Schema::dropIfExists('lister_ac_song');
    }
}
