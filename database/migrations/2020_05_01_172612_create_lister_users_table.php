<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateListerUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lister_users', function (Blueprint $table) {
            $table->id();
            $table->string('username')->unique();
            $table->string('email')->nullable();
            $table->timestamps();
        });

        // SQLite cant drop foreign keys
        if (DB::getDriverName() === 'sqlite') {
            // Remake the table
            Schema::dropIfExists('lister_ac_list');
            // Copied from migrations/2020_04_21_051132_create_lister_tables.php
            Schema::create('lister_ac_list', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('user_id');
                $table->string('pin');
                $table->timestamps();
            });
        } else {
            Schema::table('lister_ac_list', function (Blueprint $table) {
                $table->dropForeign(['user_id']);
            });
            // Required to maintain DB integrity
            DB::statement('SET FOREIGN_KEY_CHECKS=0');
            DB::table('lister_ac_list')->truncate(); //WARNING DESTROYING DATA!!
            DB::table('lister_ac_list_song')->truncate(); //WARNING DESTROYING DATA!!
            DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
        Schema::table('lister_ac_list', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('lister_users');
        });
    }

    /**
     * Reverse the migrations.
     * TODO Broken
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lister_ac_list', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });

        Schema::dropIfExists('lister_users');

        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        DB::table('lister_ac_list')->truncate(); //WARNING DESTROYING DATA!!
        DB::table('lister_ac_list_song')->truncate(); //WARNING DESTROYING DATA!!
        DB::table('fake_users')->truncate(); //WARNING DESTROYING DATA!!
        DB::statement('SET FOREIGN_KEY_CHECKS=1');

        Schema::table('lister_ac_list', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('fake_users'); // DB INTEGRITY
        });
    }
}
