<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateErrorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_logger_errors', function (Blueprint $table) {
            $table->id();
            $table->json('error');
            $table->json('extra');
            $table->dateTime('error_date');
            $table->string('app')->comment('App url or name');
            $table->string('origin');
            $table->string('ip');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_logger_errors');
    }
}
