<?php

use App\Models\Lister\ACList;
use App\Models\Lister\ACSong;
use App\Models\Lister\ACListSong;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKKJazzToPresentLists extends Migration
{
    /**
     * Run the migrations.
     * Create a ListSong entry for every list in the DB, with the KKJazz song
     *
     * @return void
     */
    public function up()
    {
        $song = ACSong::where('name', 'K.K. Jazz')->first();
        $lists = ACList::all();
        foreach ($lists as $list) {
            if ($this->containsSong($list->listSongs, $song->id)) continue;
            $listSong = new ACListSong();
            $listSong->list_id = $list->id;
            $listSong->song_id = $song->id;
            $listSong->checked = 0;
            $listSong->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $song = ACSong::where('name', 'K.K. Jazz')->first();
        $listSongs = ACListSong::all();
        foreach ($listSongs as $listSong) {
            if ($listSong->song_id === $song->id) {
                $listSong->delete();
            }
        }
    }

    protected function containsSong($listSongs, $id) {
        foreach ($listSongs as $listSong) {
            if ($listSong->song_id === $id) {
                return true;
            }
        }
        return false;
    }
}
