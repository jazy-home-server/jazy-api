<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusChecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_checks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->decimal('rtt', 8, 4)->comment('Round Trip Time in Seconds');
            $table->boolean('success');
            $table->unsignedBigInteger('check_error_id')->nullable();
            $table->foreign('check_error_id')->references('id')->on('check_errors');
            $table->unsignedBigInteger('check_type_id');
            $table->foreign('check_type_id')->references('id')->on('check_types');
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_checks');
    }
}
