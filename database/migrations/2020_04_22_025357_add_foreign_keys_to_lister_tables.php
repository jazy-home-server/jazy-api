<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToListerTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lister_ac_list', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('fake_users');
        });
        Schema::table('lister_ac_list_song', function (Blueprint $table) {
            $table->foreign('list_id')->references('id')->on('lister_ac_list')->onDelete('cascade');
            $table->foreign('song_id')->references('id')->on('lister_ac_song')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lister_ac_list', function (Blueprint $table) {
            $table->dropForeign(['user_id']);
        });
        Schema::table('lister_ac_list_song', function (Blueprint $table) {
            $table->dropForeign(['list_id']);
            $table->dropForeign(['song_id']);
        });
    }
}
