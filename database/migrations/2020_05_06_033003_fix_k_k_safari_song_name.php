<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixKKSafariSongName extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // cant believe im doing this...
        DB::table('lister_ac_song')
            ->where('name', 'K.K.Safari')
            ->update(['name' => 'K.K. Safari']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('lister_ac_song')
            ->where('name', 'K.K. Safari')
            ->update(['name' => 'K.K.Safari']);
    }
}
