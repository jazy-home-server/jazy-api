<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\Lister\ACSong;

class FixAcSongsUrls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Remove "http:" from all img urls
        $songs = ACSong::all();
        foreach($songs as $song) {
            DB::table('lister_ac_song')
                ->where('name', $song['name'])
                ->update(['img_url' => substr($song['img_url'], 5)]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $songs = ACSong::all();
        foreach($songs as $song) {
            DB::table('lister_ac_song')
                ->where('name', $song['name'])
                ->update(['img_url' => 'http:' . $song['img_url']]);
        }
    }

    public function makeSongs()
    {
        $songs = [];
        $songs[] = ['name' => 'Agent K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s58.png'];
        $songs[] = ['name' => 'Aloha K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s61.png'];
        $songs[] = ['name' => 'Animal City',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s94.png'];
        $songs[] = ['name' => 'Bubblegum K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s57.png'];
        $songs[] = ['name' => 'Cafe K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s45.png'];
        $songs[] = ['name' => 'Comrade K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s1.png'];
        $songs[] = ['name' => 'DJ K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s39.png'];
        $songs[] = ['name' => 'Drivin\'',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s95.png'];
        $songs[] = ['name' => 'Farewell',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s93.png'];
        $songs[] = ['name' => 'Forest Life',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s65.png'];
        $songs[] = ['name' => 'Go K.K. Rider',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s69.png'];
        $songs[] = ['name' => 'Hypno K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s67.png'];
        $songs[] = ['name' => 'I Love You',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s53.png'];
        $songs[] = ['name' => 'Imperial K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s2.png'];
        $songs[] = ['name' => 'K.K. Adventure',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s82.png'];
        $songs[] = ['name' => 'K.K. Aria',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s3.png'];
        $songs[] = ['name' => 'K.K. Ballad',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s81.png'];
        $songs[] = ['name' => 'K.K. Bazaar',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s62.png'];
        $songs[] = ['name' => 'K.K. Birthday',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s54.png'];
        $songs[] = ['name' => 'K.K. Blues',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s4.png'];
        $songs[] = ['name' => 'K.K. Bossa',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s5.png'];
        $songs[] = ['name' => 'K.K. Calypso',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s70.png'];
        $songs[] = ['name' => 'K.K. Casbah',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s46.png'];
        $songs[] = ['name' => 'K.K. Chorale',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s47.png'];
        $songs[] = ['name' => 'K.K. Condor',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s6.png'];
        $songs[] = ['name' => 'K.K. Country',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s41.png'];
        $songs[] = ['name' => 'K.K. Cruisin\'',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s7.png'];
        $songs[] = ['name' => 'K.K. D&B',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s90.png'];
        $songs[] = ['name' => 'K.K. Dirge',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s89.png'];
        $songs[] = ['name' => 'K.K. Disco',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s8.png'];
        $songs[] = ['name' => 'K.K. Dixie',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s79.png'];
        $songs[] = ['name' => 'K.K. Etude',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s9.png'];
        $songs[] = ['name' => 'K.K. Faire',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s42.png'];
        $songs[] = ['name' => 'K.K. Flamenco',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s10.png'];
        $songs[] = ['name' => 'K.K. Folk',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s55.png'];
        $songs[] = ['name' => 'K.K. Fusion',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s11.png'];
        $songs[] = ['name' => 'K.K. Groove',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s71.png'];
        $songs[] = ['name' => 'K.K. Gumbo',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s48.png'];
        $songs[] = ['name' => 'K.K. House',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s12.png'];
        $songs[] = ['name' => 'K.K. Island',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s13.png'];
        $songs[] = ['name' => 'K.K. Jongara',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s76.png'];
        $songs[] = ['name' => 'K.K. Lament',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s14.png'];
        $songs[] = ['name' => 'K.K. Love Song',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s64.png'];
        $songs[] = ['name' => 'K.K. Lullaby',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s15.png'];
        $songs[] = ['name' => 'K.K. Mambo',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s16.png'];
        $songs[] = ['name' => 'K.K. Marathon',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s17.png'];
        $songs[] = ['name' => 'K.K. March',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s18.png'];
        $songs[] = ['name' => 'K.K. Mariachi',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s19.png'];
        $songs[] = ['name' => 'K.K. Metal',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s85.png'];
        $songs[] = ['name' => 'K.K. Milonga',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s80.png'];
        $songs[] = ['name' => 'K.K. Moody',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s60.png'];
        $songs[] = ['name' => 'K.K. Oasis',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s20.png'];
        $songs[] = ['name' => 'K.K. Parade',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s56.png'];
        $songs[] = ['name' => 'K.K. Ragtime',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s21.png'];
        $songs[] = ['name' => 'K.K. Rally',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s87.png'];
        $songs[] = ['name' => 'K.K. Reggae',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s49.png'];
        $songs[] = ['name' => 'K.K. Rock',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s22.png'];
        $songs[] = ['name' => 'K.K. Rockabilly',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s83.png'];
        $songs[] = ['name' => 'K.K. Salsa',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s23.png'];
        $songs[] = ['name' => 'K.K. Samba',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s24.png'];
        $songs[] = ['name' => 'K.K. Ska',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s25.png'];
        $songs[] = ['name' => 'K.K. Sonata',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s43.png'];
        $songs[] = ['name' => 'K.K. Song',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s26.png'];
        $songs[] = ['name' => 'K.K. Soul',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s27.png'];
        $songs[] = ['name' => 'K.K. Steppe',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s88.png'];
        $songs[] = ['name' => 'K.K. Stroll',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s68.png'];
        $songs[] = ['name' => 'K.K. Swing',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s75.png'];
        $songs[] = ['name' => 'K.K. Synth',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s50.png'];
        $songs[] = ['name' => 'K.K. Tango',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s78.png'];
        $songs[] = ['name' => 'K.K. Technopop',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s28.png'];
        $songs[] = ['name' => 'K.K. Waltz',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s86.png'];
        $songs[] = ['name' => 'K.K. Western',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s51.png'];
        $songs[] = ['name' => 'K.K.Safari',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/kk_safari.png'];
        $songs[] = ['name' => 'King K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s77.png'];
        $songs[] = ['name' => 'Lucky K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s29.png'];
        $songs[] = ['name' => 'Marine Song 2001',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s84.png'];
        $songs[] = ['name' => 'Mountain Song',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s30.png'];
        $songs[] = ['name' => 'Mr. K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s72.png'];
        $songs[] = ['name' => 'My Place',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s31.png'];
        $songs[] = ['name' => 'Neapolitan',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s44.png'];
        $songs[] = ['name' => 'Only Me',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s32.png'];
        $songs[] = ['name' => 'Pondering',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s73.png'];
        $songs[] = ['name' => 'Rockin\' K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s33.png'];
        $songs[] = ['name' => 'Soulful K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s34.png'];
        $songs[] = ['name' => 'Space K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s66.png'];
        $songs[] = ['name' => 'Spring Blossoms',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s59.png'];
        $songs[] = ['name' => 'Stale Cupcakes',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s52.png'];
        $songs[] = ['name' => 'Steep Hill',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s91.png'];
        $songs[] = ['name' => 'Surfin\' K.K.',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s40.png'];
        $songs[] = ['name' => 'The K. Funk',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s37.png'];
        $songs[] = ['name' => 'To The Edge',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s38.png'];
        $songs[] = ['name' => 'Two Days Ago',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s63.png'];
        $songs[] = ['name' => 'Wandering',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s92.png'];
        $songs[] = ['name' => 'Welcome Horizons',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s36.png'];
        $songs[] = ['name' => 'Wild World',
        'img_url' => '//jazyserver.com/shared/lister/acmusic/s96.png'];

        return $songs;
    }
}
