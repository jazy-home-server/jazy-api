<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKKJazzSong extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('lister_ac_song')->insert([
            'name' => 'K.K. Jazz',
            'img_url' => '//jazyserver.com/shared/lister/acmusic/kk_jazz.png'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('lister_ac_song')->where('name', 'K.K. Jazz')->delete();
    }
}
