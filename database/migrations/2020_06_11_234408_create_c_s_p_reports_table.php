<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCSPReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meta_logger_csp_reports', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('document_uri')->nullable();
            $table->string('referrer')->nullable();
            $table->string('blocked_uri')->nullable();
            $table->string('violated_directive')->nullable();
            $table->string('original_policy')->nullable();
            $table->json('csp_report_payload');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meta_logger_csp_reports');
    }
}
