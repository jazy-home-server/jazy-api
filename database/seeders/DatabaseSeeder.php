<?php

namespace Database\Seeders;

use App\Note;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(NoteTableSeeder::class);
        $this->call(FakeUserSeeder::class);
        $this->call(StatusCheckSeeder::class);
        $this->call(ListerSeeder::class);
        $this->call(ListerUserSeeder::class);
        $this->call(CookieAuditSeeder::class);
        $this->call(ErrorSeeder::class);
        $this->call(CSPReportSeeder::class);
    }
}
