<?php

namespace Database\Seeders;

use App\Models\Meta\Logger\CSPReport;
use Illuminate\Database\Seeder;

class CSPReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CSPReport::factory()->count(10)->create();
    }
}
