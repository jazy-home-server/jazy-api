<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Lister\ListerUser;

class ListerUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ListerUser::factory()->count(20)->create();
    }
}
