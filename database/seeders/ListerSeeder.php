<?php

namespace Database\Seeders;

use App\Models\Lister\ListerUser;
use App\Models\Lister\ACList;
use App\Models\Lister\ACSong;
use Illuminate\Database\Seeder;
use App\Models\Lister\ACListSong;

class ListerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // wtf
        ACSong::factory()->count(5)->create();
        ListerUser::factory()->count(5)->create()->each(function ($user) {
            $userId = $user->id;
            ACList::factory()->count(mt_rand(0, 2))->create([
                'user_id' => $userId,
            ])->each(function ($list) {
                $listId = $list->id;
                ACSong::inRandomOrder()->limit(mt_rand(1, 5))->get()->each(function ($song) use ($listId) {
                    $songId = $song->id;
                    ACListSong::factory()->create([
                        'list_id' => $listId,
                        'song_id' => $songId,
                    ]);
                });
            });
        });
    }
}
