<?php

namespace Database\Seeders;

use App\Models\FakeUser;
use Illuminate\Database\Seeder;

class FakeUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FakeUser::factory()->count(10)->create();
    }
}
