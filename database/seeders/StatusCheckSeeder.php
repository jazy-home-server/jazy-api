<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\StatusCheck\CheckType;
use App\Models\StatusCheck\CheckError;
use App\Models\StatusCheck\StatusCheck;

class StatusCheckSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CheckError::factory()->count(10)->create();
        CheckType::factory()->count(10)->create();
        StatusCheck::factory()->count(200)->create();
    }
}
