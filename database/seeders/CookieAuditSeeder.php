<?php

namespace Database\Seeders;

use App\Models\Meta\CookieAudit;
use App\Models\Meta\CookieAuditAction;
use Illuminate\Database\Seeder;

class CookieAuditSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CookieAuditAction::factory()->count(10)->create();
        CookieAudit::factory()->count(10)->create([
            'action_id' => fn() => CookieAuditAction::inRandomOrder()->first()->id,
        ]);
    }
}
