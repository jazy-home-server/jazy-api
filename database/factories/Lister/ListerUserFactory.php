<?php
namespace Database\Factories\Lister;

use App\Models\Lister\ListerUser as Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ListerUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'username' => Str::of($this->faker->unique()->userName)->slug(),
            'email' => fn() => mt_rand() % 2 ? $this->faker->safeEmail : '',
        ];
    }
}
