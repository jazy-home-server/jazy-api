<?php
namespace Database\Factories\Lister;

use App\Models\Lister\ACListSong as Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class ACListSongFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'list_id' => function() {
                return \App\Models\Lister\ACList::factory()->create()->id;
            },
            'song_id' => function() {
                return \App\Models\Lister\ACSong::factory()->create()->id;
            },
            'checked' => mt_rand(0,1),
        ];
    }
}
