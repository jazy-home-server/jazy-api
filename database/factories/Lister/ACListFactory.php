<?php
namespace Database\Factories\Lister;

use App\Models\Lister\ACList as Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;

class ACListFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => function () {
                return \App\Models\Lister\ListerUser::factory()->create()->id;
            },
            'pin' => Hash::make('1234'),
        ];
    }
}
