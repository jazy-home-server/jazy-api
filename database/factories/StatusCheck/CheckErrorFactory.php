<?php
namespace Database\Factories\StatusCheck;

use App\Models\StatusCheck\CheckError as Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class CheckErrorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'exception_name' => $this->faker->word(),
            'message' => $this->faker->text(),
        ];
    }
}
