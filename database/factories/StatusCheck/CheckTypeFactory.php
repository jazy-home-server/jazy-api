<?php
namespace Database\Factories\StatusCheck;

use App\Models\StatusCheck\CheckType as Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class CheckTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(3, true),
            'description' => $this->faker->text(),
            'url' => $this->faker->url(),
        ];
    }
}
