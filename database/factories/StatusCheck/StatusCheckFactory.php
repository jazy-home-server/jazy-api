<?php
namespace Database\Factories\StatusCheck;

use App\Models\StatusCheck\StatusCheck as Model;
use Carbon\Carbon;
use App\Models\StatusCheck\CheckType;
use App\Models\StatusCheck\CheckError;
use Illuminate\Database\Eloquent\Factories\Factory;

class StatusCheckFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'success' => function () {
                // more successes than fails
                return mt_rand(0, 9) % 10;
            },
            'rtt' => rand(0, 600) / 1000,
            'check_type_id' => function () {
                $ct = CheckType::inRandomOrder()->first();
                if (empty($ct))
                    return CheckType::factory()->create()->id;
                return $ct->id;
            },
            'check_error_id' => function () {
                $ce = CheckError::inRandomOrder()->first();
                if (empty($ce))
                    return CheckError::factory()->create()->id;
                return $ce->id;
            },
            //'created_at' => Carbon::parse($this->faker->dateTimeBetween(Carbon::parse('2 hours ago')->toDateTimeString()))->toDateTimeString(),
            'created_at' => Carbon::parse($this->faker->dateTimeBetween(Carbon::parse('1 hour ago')->toDateTimeString()))->toDateTimeString(),
        ];
    }
}
