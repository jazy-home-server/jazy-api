<?php
namespace Database\Factories\Meta;

use App\Models\Meta\CookieAudit as Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class CookieAuditFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'action_id' => fn() => factory(\App\Models\Meta\CookieAuditAction::class)->create()->id,
            'origin' => $this->faker->url,
            'cookies' => [[$this->faker->word => $this->faker->word]],
        ];
    }
}
