<?php
namespace Database\Factories\Meta\Logger;

use App\Models\Meta\Logger\CSPReport as Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class CSPReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $data = [
            'document_uri' => $this->faker->url,
            'referrer' => $this->faker->url,
            'blocked_uri' => $this->faker->word,
            'violated_directive' => $this->faker->word,
            'original_policy' => $this->faker->word,
        ];
        return array_merge($data, [
            'csp_report_payload' => json_encode($data)
        ]);
    }
}
