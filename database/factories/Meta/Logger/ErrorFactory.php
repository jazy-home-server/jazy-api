<?php
namespace Database\Factories\Meta\Logger;

use App\Models\Meta\Logger\Error as Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class ErrorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'error' => json_encode(['name' => $this->faker->word, 'exception' => $this->faker->words(5, true)]),
            'extra' => json_encode(['user' => $this->faker->name, 'state' => $this->faker->words(4, true)]),
            'error_date' => $this->faker->dateTime,
            'app' => $this->faker->url,
            'origin' => $this->faker->url,
            'ip' => $this->faker->ipv4,
        ];
    }
}
