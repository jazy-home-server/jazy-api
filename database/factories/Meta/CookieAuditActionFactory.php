<?php
namespace Database\Factories\Meta;

use App\Models\Meta\CookieAuditAction as Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class CookieAuditActionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'route' => "{$this->faker->word}.{$this->faker->word}.{$this->faker->word}",
        ];
    }
}
