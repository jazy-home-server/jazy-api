module.exports = {
    testRegex: 'tests/js/.*.spec.js$',
    moduleNameMapper: {
        "@components/(.*)$": "<rootDir>/resources/js/components/$1",
    },
    setupFiles: [
        '<rootDir>/tests/js/setup.js',
    ],
    moduleFileExtensions: [
        'js',
        'json',
        'vue'
    ],
    'transform': {
        '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
        '.*\\.(vue)$': '<rootDir>/node_modules/vue-jest'
    },
}
