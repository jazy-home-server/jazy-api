# Jazy API App

Purpose of this app is to be used as my main API endpoint for other apps.

## Dev setup
I use VSCode Docker Dev Containers to spin up the php app, database, and cache services

* Run `composer install`
* Set appropriate .env variables to point to DB_HOST and REDIS_HOST
* Run `php artisan key:generate`
* Run `php artisan migrate`
* Run `php artisan queue:listen` (Must be run in independent terminal)
* Run `php artisan serve` (Must be run in independent terminal)
