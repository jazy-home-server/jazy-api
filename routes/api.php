<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/meta/gimycookie', 'Meta\\CookieAuditController@gimyCookie')
    ->name('meta.gimycookie');
Route::get('/meta/checkcookie', 'Meta\\CookieAuditController@checkCookie')
    ->name('meta.checkcookie');

Route::middleware('cors:*')
->name('meta.logger.options')
->options('meta/logger/{all}', 'MiscController@emptyOk')
->where('all', '.*');

Route::middleware('cors:*')
->name('meta.logger.')
->namespace('Meta\\Logger')
->prefix('meta/logger')
->group(function() {
    Route::get('/errors', 'ErrorController@index')
        ->name('error.index');
    Route::post('/errors', 'ErrorController@store')
        ->name('error.store');

    Route::get('/csp-reports', 'CSPReportController@index')
        ->name('csp.index');
    Route::post('/csp-reports', 'CSPReportController@store')
        ->name('csp.store');
});
