<?php

/**
 * Routes meant for testing/learning (Sort of a playground)
 */


use Illuminate\Support\Facades\Route;


Route::apiResources([
    'notes' => 'NoteController',
    'users' => 'FakeUserController',
]);

Route::name('test.options')
->middleware('cors')
->options('{all}', 'MiscController@emptyOk')
->where('all', '.*');


Route::name('lister.')
->prefix('lister')
->namespace('Lister')
->group(function() {
    Route::name('users.index')
        ->get('/users', 'ListerUserController@index');
    Route::name('users.show')
        ->get('/users/{user}', 'ListerUserController@show');
    Route::name('users.store')
        ->post('/users', 'ListerUserController@store');
    Route::name('users.update')
        ->patch('/users/{user}', 'ListerUserController@update');
    // Route::name('users.destroy')
    //     ->delete('/users/{user}', 'ListerUserController@destroy');

    Route::name('acmusic.')
    ->prefix('acmusic')
    ->group(function() {
        Route::name('lists.index')
            ->get('/lists', 'ACListController@index');
        Route::name('lists.show')
            ->get('/lists/{list}', 'ACListController@show');
        Route::name('lists.store')
            ->post('/lists', 'ACListController@store');
        Route::name('lists.update')
            ->patch('/lists/{list}', 'ACListController@update');
        // Route::name('lists.destroy')
        //     ->delete('/lists/{list}', 'ACListController@destroy');
        Route::name('lists.auth')
            ->post('/lists/{list}/auth', 'ACListController@auth');

        Route::name('listsongs.index')
            ->get('/listsongs', 'ACListSongController@index');
        Route::name('listsongs.show')
            ->get('/listsongs/{listsong}', 'ACListSongController@show');
        Route::name('listsongs.store')
            ->post('/listsongs', 'ACListSongController@store');
        Route::name('listsongs.update')
            ->patch('/listsongs/{listsong}', 'ACListSongController@update');
        // Route::name('listsongs.destroy')
        //     ->delete('/listsongs/{listsong}', 'ACListSongController@destroy');

        // Disabled all other routes for songs since they should not be changing
        Route::name('songs.index')
            ->get('/songs', 'ACSongController@index');
        Route::name('songs.show')
            ->get('/songs/{song}', 'ACSongController@show');
    });
});
