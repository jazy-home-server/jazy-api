<?php

/**
 * Routes for status check related controllers
 */


use Illuminate\Support\Facades\Route;

Route::apiResource('/statuschecks', 'StatusCheckController');
Route::apiResource('/checktypes', 'CheckTypeController');
Route::apiResource('/checkerrors', 'CheckErrorController');
