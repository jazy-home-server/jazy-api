FROM redis:5.0.7-alpine
ARG APP_NAME=jazy-api
LABEL maintainer="jazy@jazyserver.com"
LABEL com.jazyserver.api="${APP_NAME} cache"

CMD ["redis-server", "--appendonly", "yes"]
