#!/bin/sh
# This script starts a queue worker script in a daemon sort of manner

nohup sh /laravel_queue_worker_supervisor.sh > /tmp/laravel_queue_worker.log 2>&1 &
