FROM nginx:alpine
ARG APP_NAME=jazy-api
LABEL maintainer="jazy@jazyserver.com"
LABEL com.jazyserver.api="${APP_NAME} webserver"

RUN rm -rf /etc/nginx/conf.d/*
COPY ./dockerfiles/nginx/conf.d/ /etc/nginx/conf.d/
COPY ./dockerfiles/nginx/nginx.conf /etc/nginx/nginx.conf
COPY . /var/www/
EXPOSE 80
