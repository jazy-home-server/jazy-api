#!/bin/sh
# Purpose of this script is to continously run the queue worker
# The queue worker can not simply be allowed to continuously run as PHP inherintly leaks memory

php=/usr/bin/php
cd /var/www/

while true; do
    php artisan queue:work --stop-when-empty || true
    sleep 30
done
