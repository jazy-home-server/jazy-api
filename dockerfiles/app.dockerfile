# First stage to install deps
FROM php:7.4-fpm AS pre-dev
ARG APP_NAME=jazy-api
LABEL maintainer="jazy@jazyserver.com"
LABEL com.jazyserver.api="${APP_NAME}"

# Set working directory
WORKDIR /var/www

# Set timezone
RUN cp /usr/share/zoneinfo/America/New_York /etc/localtime

# Install dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    default-mysql-client \
    libzip-dev \
    libonig-dev \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    unzip \
    curl \
    cron \
    iputils-ping \
    sudo \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install pdo_mysql mbstring zip exif pcntl \
    && docker-php-ext-configure gd --with-freetype=/usr/include/ --with-jpeg=/usr/include/ \
    && docker-php-ext-install gd

# Install redis extension
RUN print "no\n" | pecl install -o -f redis \
    && rm -rf /tmp/pear \
    && docker-php-ext-enable redis

# Setup cron for laravel scheduler (Overwrite existing crontab, no need for it's contents)
RUN echo "* * * * * www-data cd /var/www/ && /usr/local/bin/php artisan schedule:run > /dev/null 2>&1" > /etc/crontab

COPY ./dockerfiles/start_queue_worker.sh /
COPY ./dockerfiles/laravel_queue_worker_supervisor.sh /
RUN chmod +x /start_queue_worker.sh
RUN chmod +x /laravel_queue_worker_supervisor.sh

EXPOSE 9000

# ------------------------------ DEV --------------------------
FROM pre-dev AS dev

# Install dev related deps
RUN apt-get update && apt-get install -y \
    git vim procps \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && composer --version

# Install NodeJS
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get install -y --no-install-recommends nodejs

# Install Yarn
# RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
#     && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
#     && apt-get update && apt-get install -y --no-install-recommends yarn=1.21.1-1 \
#     && apt-get clean && rm -rf /var/lib/apt/lists/* \
#     && yarn --version

# Install XDebug
RUN cd /usr/src \
    && curl -sS http://xdebug.org/files/xdebug-2.9.5.tgz | tar zx \
    && cd xdebug-2.9.5 \
    && phpize \
    && ./configure \
    && make \
    && cp modules/xdebug.so /usr/local/lib/php/extensions/ \
    && rm -rf /usr/src/xdebug*

# Testing
RUN touch /tmp/test.sqlite \
    && ln -s /var/www/vendor/bin/phpunit /usr/bin/phpunit

# Add user for laravel application
RUN groupadd -g 555 www \
    && useradd -u 555 -rs /bin/bash -m -g www www \
    && chown -R www:www /var/www/ \
    && echo "www ALL:(ALL) NOPASSWD: ALL" > /etc/sudoers.d/www

USER www

# Start php-fpm server
CMD ["php-fpm"]

# ------------------------ BUILD ----------------------------------
FROM dev AS build

WORKDIR /var/www

# Copy package and lock files
#COPY composer.lock composer.json yarn.lock package.json ./
COPY composer.lock composer.json package-lock.json package.json ./

# Install the laravel app dependencies (Create vendor dir)
RUN composer install --no-scripts --no-autoloader --no-dev

# Install node dependencies (Create node_modules dir)
#RUN yarn install --prod
RUN npm install

# Copy existing application directory contents
COPY . .

# Run composer autoloader
RUN composer dump-autoload --optimize

# Run laravel obtimizations
RUN php artisan optimize

# Compile js/css assets
RUN npm run prod

# ---------------------- PROD -----------------------------
FROM pre-dev AS prod

# Copy php conf file
COPY ./dockerfiles/php/conf.d/app.ini /usr/local/etc/php/conf.d/app.ini

COPY --from=build /var/www/ /var/www/

COPY ./dockerfiles/app-entrypoint.sh /entrypoint.sh

# Add user for laravel application
RUN groupadd -g 555 www \
    && useradd -u 555 -rs /bin/sh -g www www \
    && chown -R www:www /var/www/ \
    && echo "www ALL=(ALL) NOPASSWD: /usr/sbin/cron" > /etc/sudoers.d/www

USER www

ENTRYPOINT ["sh", "/entrypoint.sh"]
