FROM mysql:5.7
ARG APP_NAME=jazy-api
LABEL maintainer="jazy@jazyserver.com"
LABEL com.jazyserver.api="${APP_NAME} db"
COPY ./dockerfiles/mysql/conf.d/ /etc/mysql/conf.d/
RUN chmod 644 /etc/mysql/conf.d/*
