#!/bin/sh
set -e

echo "Starting crond..."
sudo /usr/sbin/cron

# Flush the cache
echo "Flushing the cache..."
php artisan cache:clear

# Migrate the database
echo "Migrating the DB..."
php artisan migrate --force

echo "Starting the queue worker..."
/start_queue_worker.sh

echo "Caching the /status page..."
php artisan statuschecks:cache

echo "Running php-fpm..."
php-fpm
