<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class HTTPClientServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\HTTPClient\HTTPClient',
            'App\Concretions\HTTPClient\GuzzleHTTPClient'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
