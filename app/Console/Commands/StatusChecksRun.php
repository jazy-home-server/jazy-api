<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\StatusCheck\CheckJazyHosts;
use App\Jobs\StatusCheck\CheckJazyServices;

class StatusChecksRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statuschecks:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will run a set of predefined status checks for Hosts/Websites/APIs and log them to the database. They are queued as jobs.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->b = 0;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Dispatching jobs to check jazy services...');
        dispatch(new CheckJazyServices());
        dispatch(new CheckJazyHosts());
        //(new CheckJazyServices())->handle();
        $this->comment('Jobs dispatched.');
    }
}
