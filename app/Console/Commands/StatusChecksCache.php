<?php

namespace App\Console\Commands;

use Illuminate\Http\Request;
use Illuminate\Console\Command;
use App\Http\Controllers\StatusCheck\StatusFrontendController;

class StatusChecksCache extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statuschecks:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will cache the staus page\'s data. Should only be run when the cache has been reset (app start)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->b = 0; // ???
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(StatusFrontendController $statusController)
    {
        $this->comment('Caching the status page results...');
        $statusController->basicFrontEnd(new Request());
        $this->comment('Status page cached.');
    }
}
