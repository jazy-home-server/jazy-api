<?php

/*
 * Empty wrapper for the Guzzle HTTP Client
 * Purpose is to have a client that implements the HTTPClient interface
 * (Note: Perhaps instead of creating my own interface, use GuzzleHTTP\ClientInterface ?)
 */

namespace App\Concretions\HTTPClient;

use \GuzzleHttp\Client;
use \App\Contracts\HTTPClient\HTTPClient;

class GuzzleHTTPClient extends Client implements HTTPClient
{
    private $options; // Options

    public function get($uri, array $options = []) : \Psr\Http\Message\ResponseInterface
    {
        $defaults = [
            'timeout' => 10,
            'connect_timeout' => 10,
        ];
        return parent::get($uri, array_merge($options, $defaults));
    }

    /**
     * Ping a specified host using system(ping)
     *
     * @param [string] $uri
     * @return float Ping time in seconds
     */
    public function ping(string $uri)
    {
        $host = parse_url($uri, PHP_URL_HOST) ?? $uri;
        $RTT_STRING = ' = ';

        $result = shell_exec("ping -W {$this->_testingPingTimeout} -qc1 $host 2>&1");

        // big yikes
        $rttPos = strpos($result, $RTT_STRING);
        if ($rttPos !== false)
        {
            $rttRPos = $rttPos + strlen($RTT_STRING);
            return substr($result, $rttRPos, strpos($result, '/', $rttRPos) - $rttRPos) / 1000;
        }
        else
        {
            // ping failed, throw exception
            throw new \Exception($result);
        }
    }

    public function __construct(array $options = [])
    {
        $this->_testingPingTimeout = '5';
        $this->options = $options;
        parent::__construct($this->options);
    }

    public function setOnStatsCallback($callback)
    {
        $this->options['on_stats'] = $callback;
        $this->__construct($this->options);
    }

    // TESTING
    public function setPingTimeout($t)
    {
        $this->_testingPingTimeout = $t;
    }
}
