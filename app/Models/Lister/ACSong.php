<?php

namespace App\Models\Lister;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ACSong extends Model
{
    use HasFactory;

    //
    protected $table = 'lister_ac_song';
    public $timestamps = false;

    public function listSong()
    {
        return $this->hasMany('App\Models\Lister\ACListSong', 'song_id'); // lister_ac_list_song.song_id
    }
}
