<?php

namespace App\Models\Lister;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

// More like a ListSongItem
class ACListSong extends Model
{
    use HasFactory;

    //TODO: Make 'checked' a boolean using mutators
    protected $casts  = [];
    protected $table = 'lister_ac_list_song';

    public function song()
    {
        return $this->belongsTo('App\Models\Lister\ACSong', 'song_id'); // song_id
    }

    public function list()
    {
        return $this->belongsTo('App\Models\Lister\ACList', 'list_id'); // list_id
    }


}
