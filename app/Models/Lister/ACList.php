<?php

namespace App\Models\Lister;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ACList extends Model
{
    use HasFactory;

    //
    protected $table = 'lister_ac_list';

    protected $hidden = [
        'pin',
    ];

    public function listSongs()
    {
        return $this->hasMany('App\Models\Lister\ACListSong', 'list_id'); // lister_ac_list_song.list_id
    }

    public function user()
    {
        return $this->belongsTo('App\Models\Lister\ListerUser', 'user_id'); // user_id
    }
}
