<?php

namespace App\Models\Lister;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ListerUser extends Model
{
    use HasFactory;

    protected $hidden = [
        'email',
    ];

    public function getRouteKeyName()
    {
        return 'username';
    }
}
