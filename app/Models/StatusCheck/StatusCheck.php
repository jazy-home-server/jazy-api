<?php

namespace App\Models\StatusCheck;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class StatusCheck extends Model
{
    use HasFactory;

    public $timestamps = false;
    protected $guarded = []; // ALL ATTRIBUTES ARE FILLABLE!

    // Use carbon instances
    protected $dates = [
        'created_at'
    ];

    public function checkType()
    {
        return $this->belongsTo('App\Models\StatusCheck\CheckType'); // check_type_id
    }

    public function checkError()
    {
        return $this->belongsTo('App\Models\StatusCheck\CheckError'); // check_error_id
    }
}
