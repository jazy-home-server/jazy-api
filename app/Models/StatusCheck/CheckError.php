<?php

namespace App\Models\StatusCheck;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CheckError extends Model
{
    use HasFactory;

    //
    public $timestamps = false;
    protected $guarded = []; // ALL ATTRIBUTES ARE FILLABLE!

    public function statusCheck()
    {
        return $this->hasMany('App\Models\StatusCheck\StatusCheck'); // check_error_id
    }
}
