<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class FakeUser extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    protected $hidden = [
        'password',
        'user_token',
    ];

    // public function acList()
    // {
    //     return $this->hasMany('App\Models\Lister\ACList', 'user_id'); // lister_ac_list.user_id
    // }
}
