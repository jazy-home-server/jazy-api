<?php

namespace App\Models\Meta;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CookieAudit extends Model
{
    use HasFactory;

    //
    protected $fillable = ['action_id', 'origin', 'cookies'];
    protected $casts = [
        'cookies' => 'json',
    ];
    protected $table = 'meta_cookie_audits';

    public function action()
    {
        return $this->belongsTo('App\Models\Meta\CookieAuditAction', 'action_id');
    }
}
