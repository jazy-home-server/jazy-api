<?php

namespace App\Models\Meta\Logger;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CSPReport extends Model
{
    use HasFactory;

    //
    protected $table = 'meta_logger_csp_reports';
}
