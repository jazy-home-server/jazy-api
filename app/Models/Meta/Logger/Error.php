<?php

namespace App\Models\Meta\Logger;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Error extends Model
{
    use HasFactory;

    protected $table = 'meta_logger_errors';
    protected $hidden = ['ip'];
}
