<?php

namespace App\Models\Meta;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class CookieAuditAction extends Model
{
    use HasFactory;

    // This model is meant as a de-duplicating model for CookieAudit (action_id)
    protected $table = 'meta_cookie_audit_actions';
    protected $fillable = ['route'];
    public $timestamps = false;


    public function cookieAudits()
    {
        return $this->hasMany('App\Models\Meta\CookieAudit', 'action_id');
    }
}
