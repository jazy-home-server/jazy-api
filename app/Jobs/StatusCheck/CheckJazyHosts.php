<?php

namespace App\Jobs\StatusCheck;

use App\Contracts\HTTPClient\HTTPClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\StatusCheck\Checker\ICMPChecker;

class CheckJazyHosts extends ACheckJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $description = 'Confirm that my server hosts are up';

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(HTTPClient $client)
    {
        $checks = [
            'https://home.jazyserver.com', // Duplicate
            // 'https://vps.jazyserver.com',
            'https://kvm.jazyserver.com',
        ];

        foreach ($checks as $check) {
            $checker = new ICMPChecker($client, $check);
            $checker->safeCheck();
            $this->storeCheckerInDatabase($checker);
        }

        // Check if the host this server is on can connect to the internet
        $checker = new ICMPChecker($client, '8.8.8.8', 'ICMP checker for server host (Ping 8.8.8.8)');
        $checker->safeCheck();
        $this->storeCheckerInDatabase($checker);

    }
}
