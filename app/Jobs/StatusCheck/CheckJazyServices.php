<?php

namespace App\Jobs\StatusCheck;

use App\Contracts\HTTPClient\HTTPClient;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Jobs\StatusCheck\Checker\HttpChecker;
use App\Jobs\StatusCheck\Checker\WebpageChecker;

class CheckJazyServices extends ACheckJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $description = 'Confirm that the home server is up (General)';
    public $timeout = 230;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(HTTPClient $client)
    {
        $httpChecks = [
            //'https://serviio.jazyserver.com',
            //'https://home.jazyserver.com', // Duplicate
            'https://api.jazyserver.com',
            // 'https://vps.jazyserver.com',
            'https://kvm.jazyserver.com',
            //'https://jazyllerena.com', // Duplicate
            'https://portfolio.jazyllerena.com',
            'https://mail.jazyserver.com/status.php',
            // 'https://vps.jazyserver.com/mail-status.php',
            'https://jazyserver.com/packetloss',
            'https://jazyserver.com/wedraw',
        ];

        $checker = new WebpageChecker($client, 'https://home.jazyserver.com/', 'jazy@jazyserver.com');
        $checker->safeCheck();
        $this->storeCheckerInDatabase($checker);

        /* Coupled to tests/Feature/StatusCheckCommandTest@dateTimeThisMonth */
        $checker = new WebpageChecker($client, 'https://jazyserver.com/', 'Jazy Llerena');
        $checker->safeCheck();
        $this->storeCheckerInDatabase($checker);

        $checker = new WebpageChecker($client, 'https://jazyllerena.com/contact/', 'Resume');
        $checker->safeCheck();
        $this->storeCheckerInDatabase($checker);

        foreach ($httpChecks as $check) {
            $checker = new HttpChecker($client, $check);
            $checker->safeCheck();
            $this->storeCheckerInDatabase($checker);
        }

    }
}
