<?php

namespace App\Jobs\StatusCheck\Checker;

use GuzzleHttp\TransferStats;
use App\Contracts\HTTPClient\HTTPClient;

abstract class AChecker
{
    public $success;
    public $rtt;

    public $typeName;
    public $typeUrl;
    public $typeDescription;

    public $errorName;
    public $errorMessage;

    protected $url;
    private $clientInstance;

    public function __construct(HTTPClient $client, string $url)
    {
        $this->clientInstance = $client;
        $this->typeName = class_basename($this);
        $this->url = $url;
        $this->typeUrl = $url; // ...
        $this->typeDescription = '';
        $this->client()->setOnStatsCallback([$this, 'guzzleOnStatsCallback']);
    }

    /**
     * Set the http client, used for testing
     */
    public function setClient($client)
    {
        $this->clientInstance = $client;
    }

    public function client()
    {
        return $this->clientInstance;
    }

    /* override */
    protected function check(): bool
    {
        return false;
    }

    public function safeCheck(): bool
    {
        try {
            $this->success = $this->check();
            return $this->success;
        } catch (\Exception $e) {
            $this->fillFromException($e);
        }

        return $this->success = false;
    }

    public function guzzleOnStatsCallback($stats)
    {
        $this->rtt = $stats->getTransferTime();
    }

    protected function fillFromException(\Exception $e)
    {
        $this->errorName = class_basename($e);
        $this->errorMessage = $e->getMessage();
    }
}
