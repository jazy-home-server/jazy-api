<?php

namespace App\Jobs\StatusCheck\Checker;

/*
 * Checker to test icmp endpoints such as hosts
 */
class ICMPChecker extends AChecker
{
    public function __construct($client, string $url, string $typeDesc = '')
    {
        parent::__construct($client, $url);
        $this->typeDescription = $typeDesc ?: "ICMP (Ping) checker for $this->url";
    }

    protected function check(): bool
    {
        $this->rtt = $this->client()->ping($this->url);

        return true;
    }
}
