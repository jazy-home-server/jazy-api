<?php

namespace App\Jobs\StatusCheck\Checker;

class WebpageChecker extends AChecker
{
    public function __construct($client, string $url, string $needle)
    {
        parent::__construct($client, $url);
        $this->needle = $needle;
        $this->typeDescription = "Webpage checker for $this->url, looking for :$this->needle";
    }

    protected function check(): bool
    {
        $res = $this->client()->get($this->url);
        $success = (bool) strstr($res->getBody(), $this->needle);
        if ($success === false)
        {
            $this->errorName = 'StringNotFound';
            $this->errorMessage = ":$this->needle: was not found at $this->url";
        }

        return $success;
    }
}
