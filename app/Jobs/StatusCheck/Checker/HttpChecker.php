<?php

namespace App\Jobs\StatusCheck\Checker;

/*
 * Checker to test http endpoints such as sites or APIs
 */
class HttpChecker extends AChecker
{
    public function __construct($client, string $url)
    {
        parent::__construct($client, $url);
        $this->typeDescription = "HTTP Response checker for $this->url";
    }

    protected function check(): bool
    {
        $res = $this->client()->get($this->url);
        $statusCode = $res->getStatusCode();
        if ($statusCode != '200')
        {
            $this->errorName = 'EndpointNotOK';
            $this->errorMessage = "$this->url returned an HTTP response code of $statusCode";
            return false;
        }

        return true;
    }
}
