<?php

namespace App\Jobs\StatusCheck;

use App\Models\StatusCheck\CheckType;
use App\Models\StatusCheck\CheckError;
use App\Models\StatusCheck\StatusCheck;

abstract class ACheckJob
{
    protected function storeCheckerInDatabase(Checker\AChecker $checker)
    {
        $checkError = empty($checker->errorName) ? null :
            CheckError::firstOrCreate([
                'exception_name' => $checker->errorName,
                'message' => $checker->errorMessage,
            ]);
        $checkType = CheckType::firstOrCreate([
            'name' => $checker->typeName,
            'url' => $checker->typeUrl,
            'description' => $checker->typeDescription,
        ]);
        $statusCheck = new StatusCheck([
            'success' => $checker->success,
            'rtt' => $checker->rtt ?? 1,
        ]);
        $statusCheck->checkError()->associate($checkError);
        $statusCheck->checkType()->associate($checkType);
        $statusCheck->save();
    }
}
