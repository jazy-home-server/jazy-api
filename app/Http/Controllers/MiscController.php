<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MiscController extends Controller
{
    //
    public function homePage()
    {
        // Test database connection
        $databaseConnectionOk = false;
        try {
            DB::connection()->getPdo();
            $databaseConnectionOk = true;
        } catch (\Exception $e) {
            if (config('app.debug')) {
                throw $e;
            }
        }

        // Get EUID
        $euid = posix_geteuid();

        return view('welcome')->with('databaseConnectionOk', $databaseConnectionOk)->with('euid', $euid);
    }

    public function emptyOk()
    {
        return response('', 200);
    }
}
