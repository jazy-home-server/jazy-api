<?php

namespace App\Http\Controllers;

use App\Models\FakeUser;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class FakeUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): ResourceCollection
    {
        return new ResourceCollection(FakeUser::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request): JsonResource
    {
        $request->validate([
            'email' => 'bail|required|max:255|email|unique:App\Models\FakeUser',
            'first_name' => 'required|string|max:255',
            'password' => 'required|string|min:8',
        ]);

        $user = FakeUser::create([
            'email' => $request['email'],
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'password' => Hash::make($request['password']),
        ]);

        /* PROPER WAY (No need for fillables)
        $user = new FakeUser;
        $user->email = $request['email'];
        $user->first_name = $request['first_name'];
        $user->last_name = $request['last_name'];
        $user->password = Hash::make($request['password']);

        $user->save();
        */

        return new JsonResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FakeUser  $user
     * @return \Illuminate\Http\Response
     */
    public function show(FakeUser $user): JsonResource
    {
        return new JsonResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\FakeUser  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FakeUser $user): JsonResource
    {
        $request->validate([
            'email' => 'bail|max:255|email|unique:App\Models\FakeUser',
            'first_name' => 'string|max:255',
            'password' => 'string|min:8',
        ]);

        if ($request->has('password'))
        {
            // Hash and store the new password (WARNING WTF!!!)
            $user->password = Hash::make($request['password']);
        }

        $user->fill($request->except(['password']))->save();

        return new JsonResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FakeUser  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(FakeUser $user): JsonResponse
    {
        $user->delete();

        return response()->json();
    }
}
