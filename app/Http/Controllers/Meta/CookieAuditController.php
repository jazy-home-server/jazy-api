<?php

namespace App\Http\Controllers\Meta;

use Illuminate\Http\Request;
use App\Models\Meta\CookieAudit;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Models\Meta\CookieAuditAction;
use Illuminate\Support\Facades\Cookie;

class CookieAuditController extends Controller
{
    public function gimyCookie(Request $request)
    {
        $request->validate([
            'cookie_name' => 'required|string',
            'cookie_value' => 'required|string',
        ]);

        $cookie = Cookie::make($request->cookie_name, $request->cookie_value, 0, '/meta');

        // Audit
        $caa = CookieAuditAction::firstOrCreate(['route' => Route::current()->getActionName()]);
        CookieAudit::create([
            'action_id' => $caa->id,
            'origin' => $request->header('origin', null),
            'cookies' => [[$request->cookie_name => $request->cookie_value]],
        ]);

        return (new JsonResponse($request->all()))->cookie($cookie);
    }

    public function checkCookie(Request $request)
    {
        $data = [];
        foreach($request->cookies as $cookieName => $cookieValue) {
            $data[] = [$cookieName => $cookieValue];
        }

        // Audit
        $caa = CookieAuditAction::firstOrCreate(['route' => Route::current()->getActionName()]);
        CookieAudit::create([
            'action_id' => $caa->id,
            'origin' => $request->header('origin', null),
            'cookies' => $data,
        ]);

        return new JsonResponse($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Meta\CookieAudit  $cookieAudit
     * @return \Illuminate\Http\Response
     */
    public function show(CookieAudit $cookieAudit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Meta\CookieAudit  $cookieAudit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CookieAudit $cookieAudit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Meta\CookieAudit  $cookieAudit
     * @return \Illuminate\Http\Response
     */
    public function destroy(CookieAudit $cookieAudit)
    {
        //
    }
}
