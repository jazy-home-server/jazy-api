<?php

namespace App\Http\Controllers\Meta\Logger;

use App\Http\Controllers\Controller;
use App\Models\Meta\Logger\Error;
use Illuminate\Http\Request;

class ErrorFrontendController extends Controller
{
    /**
     * Display errors on the web
     */
    public function index()
    {
        $errors = Error::orderBy('created_at', 'desc')->simplePaginate(10);
        return view('meta.errors', [
            'errors' => $errors,
        ]);
    }
}
