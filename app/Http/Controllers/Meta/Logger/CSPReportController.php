<?php

namespace App\Http\Controllers\Meta\Logger;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Meta\Logger\CSPReport;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CSPReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ResourceCollection(CSPReport::orderBy('created_at', 'desc')->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataJson = $request->getContent();
        $data = json_decode($dataJson, true);
        $reportData = $data['csp-report'] ?? $data;

        $report = new CSPReport();
        $report->csp_report_payload = $dataJson;
        $report->document_uri = $reportData['document-uri'];
        $report->referrer = $reportData['referrer'];
        $report->blocked_uri = $reportData['blocked-uri'];
        $report->violated_directive = $reportData['violated-directive'];
        $report->original_policy = $reportData['original-policy'];

        $report->save();
        return new JsonResource($report);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Meta\Logger\CSPReport  $cSPReport
     * @return \Illuminate\Http\Response
     */
    public function show(CSPReport $cSPReport)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Meta\Logger\CSPReport  $cSPReport
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CSPReport $cSPReport)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Meta\Logger\CSPReport  $cSPReport
     * @return \Illuminate\Http\Response
     */
    public function destroy(CSPReport $cSPReport)
    {
        //
    }
}
