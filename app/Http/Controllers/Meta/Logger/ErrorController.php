<?php

namespace App\Http\Controllers\Meta\Logger;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\Meta\Logger\Error;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ErrorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ResourceCollection(Error::orderBy('created_at', 'desc')->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'error' => 'required|json',
            'extra' => 'required|json',
            'error_date' => 'required|date',
            'app' => 'required|max:255',
        ]);

        $origin = $request->header('origin', 'Unknown');

        $error = new Error();
        $error->error = $request->error;
        $error->extra = $request->extra;
        $error->error_date = Carbon::parse($request->error_date);
        $error->app = $request->app;
        $error->ip = $request->header('x-forwarded-for', $request->ip());
        $error->origin = $origin;

        $error->save();

        return new JsonResource($error);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Meta\Error  $error
     * @return \Illuminate\Http\Response
     */
    public function show(Error $error)
    {
        // DISABLED
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Meta\Error  $error
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Error $error)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Meta\Error  $error
     * @return \Illuminate\Http\Response
     */
    public function destroy(Error $error)
    {
        //
    }
}
