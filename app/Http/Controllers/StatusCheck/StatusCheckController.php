<?php

namespace App\Http\Controllers\StatusCheck;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StatusCheck\StatusCheck;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class StatusCheckController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'expand' => 'string',
        ]);

        $response = StatusCheck::orderBy('created_at', 'desc');
        $appends = [];

        $appends['expand'] = $request->expand;
        $expands = explode(',', $request->expand);
        foreach ($expands as $expand) {
            switch ($expand) {
                case 'check_type':
                    $response = $response->with('CheckType');
                    break;
                case 'check_error':
                    $response = $response->with('CheckError');
                    break;
                default:
                    break;
            }
        }

        return new ResourceCollection($response->simplePaginate()->appends($appends));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatusCheck\StatusCheck  $statuscheck
     * @return \Illuminate\Http\Response
     */
    public function show(StatusCheck $statuscheck)
    {
        return new JsonResource($statuscheck);
    }
}
