<?php

namespace App\Http\Controllers\StatusCheck;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StatusCheck\CheckType;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CheckTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ResourceCollection(CheckType::paginate());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatusCheck\CheckType  $checktype
     * @return \Illuminate\Http\Response
     */
    public function show(CheckType $checktype)
    {
        return new JsonResource($checktype);
    }
}
