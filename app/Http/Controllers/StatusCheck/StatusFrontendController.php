<?php

namespace App\Http\Controllers\StatusCheck;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StatusCheck\CheckType;
use Illuminate\Support\Facades\Cache;
use App\Models\StatusCheck\StatusCheck;
use Illuminate\Database\Eloquent\Collection;

class StatusFrontendController extends Controller
{
    public function basicFrontEnd(Request $request)
    {
        $laravelStart = microtime(true);
        $frontendTimezone = 'America/New_York'; // TODO Create parameter

        $checksForTypes = Cache::remember('last_hour_status', 300, function() {
            return $this->fetchStatusChecksForAllTypes();
        });
        $lastUpdate = Cache::remember('last_hour_status_created_at', 300, function() {
            return Carbon::now();
        });

        return view('statuscheck')
            ->with('types', $checksForTypes)
            ->with('lastUpdate', $lastUpdate)
            ->with('laravelStart', $laravelStart)
            ->with('frontendTimezone', $frontendTimezone);
    }

    /*
     *
     * Disgusting function that retrieves the latest and last status checks for each statustype
     * The function takes a crazy amount of time (considering the amount of data produced by constant 5min status checks)
     * Therefore there are a lot of optimization efforts, using the cache, below, which also make the code harder to follow.
     * The LastFail and LastSuccess are what takes the longest, since a service may have not failed in a really long time
     *
     */
    private function fetchStatusChecksForAllTypes() {
        // fetch last hour of status checks types
        $from = Carbon::parse('1 hour ago')->toDateTimeString();
        $to = Carbon::now()->toDateTimeString();
        $data = CheckType::with(['StatusCheck.CheckError', 'StatusCheck' => function ($query) use ($from, $to) {
            $query->whereBetween('created_at', [$from, $to])->orderBy('created_at', 'desc');
        }])->get();

        // Set the last failure/success for each type MASSIVE SHITSTORM BELOW
        foreach ($data as $type) {
            if ($type->StatusCheck->isEmpty()) {
                //continue;
            }


            $type['lastFail'] = $this->fetchLastFailForStatusType($type);
            $type['lastSuccess'] = $this->fetchLastSuccessForStatusType($type);
        }

        return $data;
    }

    /*
     * @param $type: StatusType object
     */
    private function fetchLastFailForStatusType($type) {
        // Check for last fail
        $lastFailCacheKey = 'check_type_' . $type->id . '_last_fail';
        // store the ID from which the StatusChecks were checked from (so should be latest StatusCheck->id)
        // this ensures that the search for the last fail, will only be till the start of the last time it was searched for
        $lastFailSinceIdCacheKey = 'check_type_' . $type->id . '_last_fail_since_id';
        $lastFail = null;
        // $lastFail = [];
        // see if type has failed recently (always runs since its quick)
        foreach ($type->StatusCheck as $check) {
            // assuming ordered by date desc
            if (! $check->success) {
                $lastFail = $check;
                break;
            }
        }
        // if no recents, check cache, otherwise fetch it
        if (empty($lastFail)) {
            $lastFailFromCache = Cache::get($lastFailCacheKey);
            $lastFailSinceIdFromCache = Cache::get($lastFailSinceIdCacheKey);
            if (empty($lastFailSinceIdFromCache)) {
                // assuming if lastfailsinceid is empty then lastfailfromcache is empty (sinceid should be set on every run)
                // this query takes HELLA LONG (should only run on first request)
                $lastFail = StatusCheck::where('check_type_id', $type->id)
                    ->where('success', '0')
                    ->orderBy('created_at', 'desc')
                    ->first();
            } else {
                // optimize: only search from using the cached created_at
                $lastFail = StatusCheck::where('check_type_id', $type->id)
                    ->where('success', '0')
                    ->where('id', '>', $lastFailSinceIdFromCache)
                    ->orderBy('created_at', 'desc')
                    ->first();
                if (empty($lastFail)) {
                    $lastFail = $lastFailFromCache;
                }
            }
        }
        //var_dump($type->StatusCheck);
        // cache the result
        $lastStatusCheck = StatusCheck::latest()->first();
        Cache::put($lastFailCacheKey, $lastFail);
        Cache::put($lastFailSinceIdCacheKey, $lastStatusCheck->id);
        return $lastFail;
    }

    // The following function should mirror the logic of the LastFail function logic above (Hence no comments)
    private function fetchLastSuccessForStatusType($type) {
        $lastSuccessCacheKey = 'check_type_' . $type->id . '_last_success';
        $lastSuccessSinceIdCacheKey = 'check_type_' . $type->id . '_last_success_since_id';
        $lastSuccess = null;
        //$lastSuccess = []
        foreach ($type->StatusCheck as $check) {
            if ($check->success) {
                $lastSuccess = $check;
                break;
            }
        }

        if (empty($lastSuccess)) {
            $lastSuccessFromCache = Cache::get($lastSuccessCacheKey);
            $lastSuccessSinceIdFromCache = Cache::get($lastSuccessSinceIdCacheKey);
            if (empty($lastSuccessSinceIdFromCache)) {
                $lastSuccess = StatusCheck::where('check_type_id', $type->id)
                    ->where('success', '<>', '0')
                    ->orderBy('created_at', 'desc')
                    ->first();
            } else {
                $lastSuccess = StatusCheck::where('check_type_id', $type->id)
                    ->where('success', '<>', '0')
                    ->where('id', '>', $lastSuccessSinceIdFromCache)
                    ->orderBy('created_at', 'desc')
                    ->first();
                if (empty($lastSuccess)) {
                    $lastSuccess = $lastSuccessFromCache;
                }
            }
        }

        $lastStatusCheck = StatusCheck::latest()->first();
        Cache::put($lastSuccessCacheKey, $lastSuccess);
        Cache::put($lastSuccessSinceIdCacheKey, $lastStatusCheck->id);
        return $lastSuccess;
    }
}
