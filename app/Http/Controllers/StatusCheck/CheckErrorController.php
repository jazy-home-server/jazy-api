<?php

namespace App\Http\Controllers\StatusCheck;

use App\Http\Controllers\Controller;
use App\Models\StatusCheck\CheckError;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CheckErrorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ResourceCollection(CheckError::paginate());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\StatusCheck\CheckError  $checkerror
     * @return \Illuminate\Http\Response
     */
    public function show(CheckError $checkerror)
    {
        return new JsonResource($checkerror);
    }
}
