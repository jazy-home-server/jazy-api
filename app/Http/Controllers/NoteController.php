<?php

namespace App\Http\Controllers;

use App\Models\Note;
use App\Http\Resources\NoteResource;
use App\Http\Resources\NoteResourceCollection;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    /**
     * show a singular note
     *
     * @param App\Models\Note $note
     *
     * @return NoteResource
     */
    public function show(Note $note): NoteResource {
      return new NoteResource($note);
    }

    /**
     * index
     *
     * @return NoteResourceCollection
     */
    public function index(): NoteResourceCollection {
      return new NoteResourceCollection(Note::paginate());
    }

    /**
     * store
     *
     * @param Request $request
     *
     * @return void
     */
    public function store(Request $request): NoteResource {
      $request->validate([
        'title' => 'required',
        'text' => 'required',
      ]);

      $note = Note::create($request->all());

      return new NoteResource($note);
    }

    /**
     * update
     *
     * @param Request $request
     * @param Note $note
     *
     * @return NoteResource
     */
    public function update(Request $request, Note $note): NoteResource {
      $note->update($request->all());

      return new NoteResource($note);
    }

    public function destroy(Note $note) {
      $note->delete();

      return response()->json();
    }
}
