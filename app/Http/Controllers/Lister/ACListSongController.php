<?php

namespace App\Http\Controllers\Lister;

use Illuminate\Http\Request;
use App\Models\Lister\ACList;
use Illuminate\Validation\Rule;
use App\Models\Lister\ACListSong;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ACListSongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'expand' => 'string|max:255|in:song',
            'list_id' => 'string|max:255|exists:App\Models\Lister\ACList,id',
        ]);

        $listSongs = ACListSong::query();
        $pagCount = $request->pag_count ?? 10;
        $appends = ['pag_count' => $pagCount];

        if ($request->list_id) {
            $listSongs->where('list_id', $request->list_id);
            $appends['list_id'] = $request->list_id;
        }

        switch ($request->expand) {
            case 'song':
                $listSongs->with('song');
                $appends['expand'] = $request->expand;
                break;
        }

        return new ResourceCollection($listSongs->paginate($pagCount)->appends($appends));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Check that the list pin was entered and correct
        $request->validate([
            'list_id' => 'bail|required|integer|exists:App\Models\Lister\ACList,id',
            'song_id' => 'bail|required|integer|exists:App\Models\Lister\ACSong,id',
            'checked' => 'required|integer',
        ]);
        // Validate composite key
        $messages = [
            'unique' => 'List Song entry already exists.',
        ];
        Validator::make($request->all(), [
            'list_id' =>
                Rule::unique('lister_ac_list_song')->where(function ($query) use ($request) {
                    return $query->where('song_id', $request->song_id);
                }),
            'song_id' =>
                Rule::unique('lister_ac_list_song')->where(function ($query) use ($request) {
                    return $query->where('list_id', $request->list_id);
                })
        ], $messages)->validate();

        $listSong = new ACListSong;
        $listSong->list_id = $request->list_id;
        $listSong->song_id = $request->song_id;
        $listSong->checked = $request->checked;
        $listSong->save();

        return new JsonResource($listSong);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lister\ACListSong  $aCListSong
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, ACListSong $listsong)
    {
        $request->validate([
            'expand' => 'string|max:255|in:song',
        ]);

        switch ($request->expand) {
            case 'song':
                $listsong->load('song');
                break;
        }

        return new JsonResource($listsong);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lister\ACListSong  $aCListSong
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ACListSong $listsong)
    {
        $request->validate([
            'pin' => 'required|string',
            'checked' => 'integer',
        ]);

        if (!Hash::check($request->pin, $listsong->list->pin)) {
            return response()->json(['message' => 'Invalid/Incorrect pin'], 401);
        }

        $listsong->checked = $request->checked ?? $listsong->checked;
        $listsong->save();

        return new JsonResource($listsong);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lister\ACListSong  $aCListSong
     * @return \Illuminate\Http\Response
     */
    public function destroy(ACListSong $listsong)
    {
        if (!App::runningUnitTests()) return response()->json([], 401);
        $listsong->delete();

        return response()->json();
    }
}
