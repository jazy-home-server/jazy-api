<?php

namespace App\Http\Controllers\Lister;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Lister\ACList;
use App\Models\Lister\ACSong;
use App\Models\Lister\ACListSong;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ACListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->validate([
            'user_id' => 'string|max:255',
            'pag_count' => 'integer',
        ]);

        $lists = ACList::query();
        $pagCount = $request->pag_count ?? 10;
        $appends = ['pag_count' => $pagCount];

        if ($request->user_id) {
            $lists->where('user_id', $request->user_id);
            $appends['user_id'] = $request->user_id;
        }

        if ($lists->get()->count() === 0)
            return response()->json(['message' => 'No lists found'], 404);

        return new ResourceCollection($lists->paginate($pagCount)->appends($appends));
    }

    /**
     * Store a newly created resource in storage.
     * ALSO create all the ListSong entries for each Song
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // One list per user enforced here...
        $messages = [
            'user_id.unique' => 'User already has a list',
        ];

        Validator::make($request->all(), [
            'user_id' => 'bail|required|integer|max:255|unique:App\Models\Lister\ACList,user_id|exists:App\Models\Lister\ListerUser,id',
            'pin' => 'required|string|min:4',
        ], $messages)->validate();

        $list = new ACList;
        $list->user_id = $request->user_id;
        $list->pin = Hash::make($request->pin);

        // Create all the ListSong entries for each song
        DB::transaction(function () use ($list) {
            $list->save();
            $songs = ACSong::all();
            foreach ($songs as $song) {
                $listSong = new ACListSong();
                $listSong->list_id = $list->id;
                $listSong->song_id = $song->id;
                $listSong->checked = '0';
                $listSong->save();
            }
        });

        if ($list->id)
            return new JsonResource($list);
        return response()->json(['message' => 'Server error when creating the list'], 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lister\ACList  $aCList
     * @return \Illuminate\Http\Response
     */
    public function show(ACList $list)
    {
        return new JsonResource($list);
    }

    /**
     * Check authentication of pin, on list
     *
     * @param ACList $list
     * @return JsonResponse
     */
    public function auth(Request $request, ACList $list)
    {
        $request->validate([
            'pin' => 'required|string|min:4',
        ]);

        if (!Hash::check($request->pin, $list->pin)) {
            return response()->json(['message' => 'Invalid/Incorrect pin'], 401);
        }

        return new JsonResource($list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lister\ACList  $aCList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ACList $list)
    {
        $request->validate([
            'newpin' => 'required|string|min:4',
            'oldpin' => 'required|string|min:4',
        ]);

        if (!Hash::check($request->oldpin, $list->pin)) {
            return response()->json(['message' => 'Invalid/Incorrect pin'], 401);
        }

        $list->pin = Hash::make($request->newpin);
        $list->save();

        return new JsonResource($list);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lister\ACList  $aCList
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, ACList $list)
    {
        $request->validate([
            'pin' => 'required|string|min:4',
        ]);

        if (!Hash::check($request->pin, $list->pin)) {
            return response()->json(['message' => 'Invalid/Incorrect pin'], 401);
        }

        $list->delete();

        return response()->json();
    }
}
