<?php

namespace App\Http\Controllers\Lister;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Lister\ListerUser;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ListerUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ResourceCollection(ListerUser::paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge([
            'username' => Str::slug($request->username)
        ]);
        $request->validate([
            'username' => 'bail|required|string|min:4|unique:App\Models\Lister\ListerUser,username',
            'email' => 'string|email',
        ]);

        $user = new ListerUser();
        $user->username = $request->username;
        $user->email = $request->email ?: '';
        $user->save();

        return new JsonResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lister\ListerUser  $listerUser
     * @return \Illuminate\Http\Response
     */
    public function show(ListerUser $user)
    {
        return new JsonResource($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lister\ListerUser  $listerUser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ListerUser $user)
    {
        $request->merge([
            'username' => Str::slug($request->username)
        ]);
        $request->validate([
            'username' => 'bail|string|min:4|unique:App\Models\Lister\ListerUser,username',
            'email' => 'string|email',
        ]);

        // Disabled username updating due to lack of auth
        //$user->username = $request->username ?: $user->username;
        $user->email = $request->email ?: $user->email;
        $user->save();

        return new JsonResource($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lister\ListerUser  $listerUser
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListerUser $listerUser)
    {
        return response()->json([], 401);
    }
}
