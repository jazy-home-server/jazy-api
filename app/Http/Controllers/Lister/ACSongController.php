<?php

namespace App\Http\Controllers\Lister;

use Illuminate\Http\Request;
use App\Models\Lister\ACSong;
use App\Http\Controllers\Controller;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ACSongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return new ResourceCollection(ACSong::paginate());
        return new ResourceCollection(ACSong::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'img_url' => 'required|string|max:255'
        ]);

        $song = new ACSong;
        $song->name = $request->name;
        $song->img_url = $request->img_url;

        $song->save();

        return new JsonResource($song);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lister\ACSong  $aCSong
     * @return \Illuminate\Http\Response
     */
    public function show(ACSong $song)
    {
        return new JsonResource($song);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lister\ACSong  $aCSong
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ACSong $song)
    {
        $request->validate([
            'name' => 'string|max:255',
            'img_url' => 'string|max:255',
        ]);

        $song->name = $request->name ?? $song->name;
        $song->img_url = $request->img_url ?? $song->img_url;

        $song->save();

        return new JsonResource($song);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lister\ACSong  $aCSong
     * @return \Illuminate\Http\Response
     */
    public function destroy(ACSong $song)
    {
        $song->delete();

        return response()->json();
    }
}
