<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Str;

class CORS
{
    /**
     * Handle an incoming request.
     * Set the CORS header for any jazyserver.com domain
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $override = null)
    {
        $requestOrigin = $request->header('origin');
        $requestHostname = parse_url($requestOrigin, PHP_URL_HOST) ?: '';

        if (preg_match("#^[^.]*\.?jazyserver\.com$#", $requestHostname) === 1) {
            return $next($request)
                ->header('Access-Control-Allow-Origin', $requestOrigin)
                ->header('Vary', 'Origin')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
                ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');
        }

        // Testing
        if (config('app.env') === 'local') {
            return $next($request)
                ->header('Access-Control-Allow-Origin', '*')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
                ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');
        }

        if (!empty($override)) {
            return $next($request)
                ->header('Access-Control-Allow-Origin', $override)
                ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS')
                ->header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, X-Token-Auth, Authorization');
        }

        return $next($request);
    }
}
