<?php

/*
 * Purpose of this contract is to provide a basic HTTP client to the API that can be easily mocked
 * or have it's implementation replaced
 */

namespace App\Contracts\HTTPClient;

interface HTTPClient
{
    public function __construct(array $a);
    public function get(string $uri, array $a = []);
    public function ping(string $uri);
    public function setOnStatsCallback($callback);
}
