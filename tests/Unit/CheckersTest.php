<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use Tests\Mocks\MockeryClientFactory;
use App\Jobs\StatusCheck\Checker\HttpChecker;
use App\Jobs\StatusCheck\Checker\WebpageChecker;

class CheckersTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testWebpageChecker()
    {
        $testNeedle = 'testString';
        $testUrl = 'test.test.com';

        $clientFactory = new MockeryClientFactory();
        $mockClient = $clientFactory
            ->responseBody('blah' . $testNeedle . 'blah')->responseStatusCode('200')->createClient();

        $checker = new WebpageChecker($mockClient, $testUrl, $testNeedle);

        $success = $checker->safeCheck();

        $this->assertTrue($success, 'Checker returned success');
        $this->assertEquals($checker->typeName, class_basename($checker), 'Checker type name is its classname');
        $this->assertEquals($checker->typeUrl, $testUrl, 'Checker type url is correct');
        $this->assertEmpty($checker->errorName, 'Checker error null');
        $this->assertNotFalse(strstr($checker->typeDescription, $testNeedle), 'Checker description has needle');
        $this->assertNotFalse(strstr($checker->typeDescription, $testUrl), 'Checker description has url');
    }

    public function testWebpageCheckerFail()
    {
        $testNeedle = 'testString';
        $testUrl = 'test.test.com';

        $clientFactory = new MockeryClientFactory();
        $mockClient = $clientFactory
            ->responseBody('blahblah')->responseStatusCode('200')->createClient();

        $checker = new WebpageChecker($mockClient, $testUrl, $testNeedle);

        $success = $checker->safeCheck();

        $this->assertFalse($success, 'Checker returned success');
        $this->assertEquals($checker->typeName, class_basename($checker), 'Checker type name is its classname');
        $this->assertEquals($checker->typeUrl, $testUrl, 'Checker type url is correct');
        $this->assertEquals('StringNotFound', $checker->errorName, 'Checker error is StringNotFound');
        $this->assertNotFalse(strstr($checker->typeDescription, $testNeedle), 'Checker description has needle');
        $this->assertNotFalse(strstr($checker->typeDescription, $testUrl), 'Checker description has url');
    }

    public function testWebpageCheckerException()
    {
        $testNeedle = 'testString';
        $testUrl = 'test.test.com';
        $testErrorMessage = 'TestError';
        $exception = new \Exception($testErrorMessage);

        $clientFactory = new MockeryClientFactory();
        $mockClient = $clientFactory
            ->responseBody('blahblah')->responseStatusCode('200')->shouldThrow($exception)->createClient();

        $checker = new WebpageChecker($mockClient, $testUrl, $testNeedle);

        $success = $checker->safeCheck();

        $this->assertFalse($success, 'Checker returned success');
        $this->assertEquals($checker->typeName, class_basename($checker), 'Checker type name is its classname');
        $this->assertEquals($checker->typeUrl, $testUrl, 'Checker type url is correct');
        $this->assertEquals('Exception', $checker->errorName, 'Checker error is Exception');
        $this->assertEquals($testErrorMessage, $checker->errorMessage, 'Checker error has message');
        $this->assertNotFalse(strstr($checker->typeDescription, $testNeedle), 'Checker description has needle');
        $this->assertNotFalse(strstr($checker->typeDescription, $testUrl), 'Checker description has url');
    }

    public function testHttpChecker()
    {
        $testUrl = 'test.test.com';

        $clientFactory = new MockeryClientFactory();
        $mockClient = $clientFactory
            ->responseBody('blahblah')->responseStatusCode('200')->createClient();

        $checker = new HttpChecker($mockClient, $testUrl);

        $success = $checker->safeCheck();

        $this->assertTrue($success, 'Checker returned success');
        $this->assertEquals($checker->typeName, class_basename($checker), 'Checker type name is its classname');
        $this->assertEquals($checker->typeUrl, $testUrl, 'Checker type url is correct');
        $this->assertEmpty($checker->errorName, 'Checker error null');
        $this->assertNotFalse(strstr($checker->typeDescription, $testUrl), 'Checker description has url');
    }

    public function testHttpCheckerFail()
    {
        $testUrl = 'test.test.com';

        $clientFactory = new MockeryClientFactory();
        $mockClient = $clientFactory
            ->responseStatusCode('404')->createClient();

        $checker = new HttpChecker($mockClient, $testUrl);

        $success = $checker->safeCheck();

        $this->assertFalse($success, 'Checker returned success');
        $this->assertEquals($checker->typeName, class_basename($checker), 'Checker type name is its classname');
        $this->assertEquals($checker->typeUrl, $testUrl, 'Checker type url is correct');
        $this->assertEquals('EndpointNotOK', $checker->errorName, 'Checker error is EndpointNotOK');
        $this->assertNotFalse(strstr($checker->typeDescription, $testUrl), 'Checker description has url');
    }

}
