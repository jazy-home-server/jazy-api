<?php

namespace Tests\Unit;

use App\Concretions\HTTPClient\GuzzleHTTPClient;
use PHPUnit\Framework\TestCase;

class PingCheckerTest extends TestCase
{
    /**
     * Test my adapted ping method on the guzzle http client concretion
     *
     * @return void
     */
    public function testGuzzleHTTPClientPingSuccess()
    {
        $client = new GuzzleHTTPClient();
        $result = $client->ping('localhost');

        $this->assertIsNumeric($result, 'Ping result is numeric');
        $this->assertTrue($result < 1, 'Ping result is less than 1');
    }
    public function testGuzzleHTTPClientPingFailure()
    {
        $HOST = 'localhostt';
        $client = new GuzzleHTTPClient();
        $client->setPingTimeout('1');
        try {
            $client->ping($HOST);
            $this->assertTrue(false, 'Exception was not thrown');
        } catch (\Exception $e) {
            $this->assertStringContainsString($HOST, $e->getMessage(), 'Error doesnt contain pinged host name');
        }
    }
    public function testGuzzleHTTPClientPingFailureTimeout()
    {
        $HOST = '2.2.2.2';
        $client = new GuzzleHTTPClient();
        $client->setPingTimeout('1');
        try {
            $client->ping($HOST);
            $this->assertTrue(false, 'Exception was not thrown');
        } catch (\Exception $e) {
            $this->assertStringContainsString($HOST, $e->getMessage(), 'Error doesnt contain pinged host name');
            $this->assertStringNotContainsString(' = ', $e->getMessage(), 'Error shows packetloss');
        }
    }
}
