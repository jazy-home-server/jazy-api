<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Http\Middleware\CORS;

/**
 * Purpose of these tests are basically to test the regularexpression within
 * the CORS middleware. Kinda pointless honestly but I just wanted to make sure
 * the php regex wasn't doing any funny bussiness. Also if I ever need to change
 * the regex, at least I have these test cases for a sanity check.
 */

class FakeRequest
{
    protected $headers = [];

    public function setHeader($header, $value)
    {
        $this->headers[$header] = $value;
    }
    public function header($header, $value = null)
    {
        if ($value !== null) {
            $this->setHeader($header, $value);
            return $this;
        }

        if (array_key_exists($header, $this->headers))
            return $this->headers[$header];
        return null;
    }
}

class CORSMiddlewareTest extends TestCase
{
    protected function _checkCorsHeaderForOrigin($origin)
    {
        $corsMid = new CORS();
        $fakeRequest = new FakeRequest();
        $fakeNext = function($request) {
            return $request;
        };

        $fakeRequest->setHeader('origin', $origin);

        $corsMid->handle($fakeRequest, $fakeNext);

        $this->assertEquals($fakeRequest->header('Access-Control-Allow-Origin'), $origin, 'ACAO header was not set');
    }
    protected function _checkCorsHeaderMissing($origin)
    {
        $corsMid = new CORS();
        $fakeRequest = new FakeRequest();
        $fakeNext = function($request) {
            return $request;
        };

        $fakeRequest->setHeader('origin', $origin);

        $corsMid->handle($fakeRequest, $fakeNext);

        $this->assertEquals($fakeRequest->header('Access-Control-Allow-Origin'), null
            , 'ACAO header was set to ' . $fakeRequest->header('Access-Control-Allow-Origin'));
    }

    public function testCorsForLocal()
    {
        config(['app.env' => 'local']);
        $origin = 'http://localhost:9000/poop';
        $corsMid = new CORS();
        $fakeRequest = new FakeRequest();
        $fakeNext = function($request) {
            return $request;
        };

        $fakeRequest->setHeader('origin', $origin);

        $corsMid->handle($fakeRequest, $fakeNext);

        $this->assertEquals($fakeRequest->header('Access-Control-Allow-Origin'), '*'
            , 'ACAO header was set to ' . $fakeRequest->header('Access-Control-Allow-Origin'));
    }

    public function testCorsValidSubDomain()
    {
        $origin = 'http://test.jazyserver.com';
        $this->_checkCorsHeaderForOrigin($origin);
    }
    public function testCorsValidSubDomainWithPath()
    {
        $origin = 'http://test.jazyserver.com/asdas/dasda';
        $this->_checkCorsHeaderForOrigin($origin);
    }
    public function testCorsValidSubDomainWithPathHyphen()
    {
        $origin = 'http://dev-test.jazyserver.com/asdas/dasda';
        $this->_checkCorsHeaderForOrigin($origin);
    }
    public function testCorsValidMainDomain()
    {
        $origin = 'http://jazyserver.com';
        $this->_checkCorsHeaderForOrigin($origin);
    }
    public function testCorsValidMainDomainWithPath()
    {
        $origin = 'http://jazyserver.com/fasdfjsf';
        $this->_checkCorsHeaderForOrigin($origin);
    }
    public function testCorsMissingMischiv1()
    {
        $origin = 'http://fake.misc.jazyserver.com/fasdfjsf';
        $this->_checkCorsHeaderMissing($origin);
    }
    public function testCorsMissingMischiv2()
    {
        $origin = 'http://fake.jazyserver.misc.com/fasdfjsf';
        $this->_checkCorsHeaderMissing($origin);
    }
    public function testCorsMissingMischiv3()
    {
        $origin = 'http://jazyserver.fake.com/fas/jazyserver.com/dfjsf';
        $this->_checkCorsHeaderMissing($origin);
    }
    public function testCorsMissingMischiv4()
    {
        $origin = 'http://jazyserver.fake.com/fasd/fake.jazyserver.com/fjsf';
        $this->_checkCorsHeaderMissing($origin);
    }
    public function testCorsMissingMischiv5()
    {
        $origin = 'http://jazyserver.fake/fasdfjsf';
        $this->_checkCorsHeaderMissing($origin);
    }
}
