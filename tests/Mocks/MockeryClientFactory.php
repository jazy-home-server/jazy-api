<?php

namespace Tests\Mocks;

use Mockery;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use App\Contracts\HTTPClient\HTTPClient;

class MockeryClientFactory
{
    protected $_resBody;
    protected $_statusCode;
    protected $_resRtt;
    protected $_exception;

    public function __construct()
    {
        $this->resBody = 'test';
        $this->statusCode = '200';
        $this->resRtt = 0.5;
    }
    function responseBody(string $resBody)
    {
        $this->_resBody = $resBody;
        return $this;
    }
    function responseStatusCode(string $statusCode)
    {
        $this->_statusCode = $statusCode;
        return $this;
    }
    function rtt($rtt)
    {
        $this->_rtt = $rtt;
        return $this;
    }
    function shouldThrow($exception)
    {
        $this->_exception = $exception;
        return $this;
    }

    public function createClient()
    {
        $this->responseMock = \Mockery::mock(\Psr\Http\Message\ResponseInterface::class);
        $this->responseMock->shouldReceive('getBody')->andReturn($this->_resBody);
        $this->responseMock->shouldReceive('getStatusCode')->andReturn($this->_statusCode);

        /**
         * Required to shut vscode intelephense about it's type being a string
         * @var \Psr\Http\Message\RequestInterface $requestInterface
        */
        $this->requestInterface = \Mockery::mock(\Psr\Http\Message\RequestInterface::class);

        $this->transferStats = \Mockery::mock(
            new \GuzzleHttp\TransferStats(
                $this->requestInterface
            )
        );
        $this->transferStats->shouldReceive('getTransferTime')->andReturn(function () {
            return $this->_rtt;
        });

        $clientMock = $this->mockClient();

        return $clientMock;
    }

    protected function mockClient()
    {
        $clientMock = \Mockery::mock(HTTPClient::class);

        if (empty($this->_exception)) {
            $clientMock->shouldReceive('get')->andReturn($this->responseMock);
        } else {
            $clientMock->shouldReceive('get')->andThrow($this->_exception);
        }

        $clientMock->shouldReceive('setOnStatsCallback')
            ->andReturn(function($callback) {
                call_user_func($callback, $this->transferStats);
            });

        return $clientMock;
    }
}
