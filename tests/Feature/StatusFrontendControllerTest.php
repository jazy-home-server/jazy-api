<?php

namespace Tests\Feature;

use App\Models\StatusCheck\CheckError;
use App\Models\StatusCheck\CheckType;
use Tests\TestCase;
use Illuminate\Support\Facades\Cache;
use App\Models\StatusCheck\StatusCheck;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatusFrontendControllerTest extends TestCase
{
    private $type;
    private $check;

    public function setUp(): void
    {
        parent::setUp();
        $this->type = CheckType::factory()->create();
        $this->check = StatusCheck::factory()->create([
            'success' => false,
            'check_type_id' => $this->type->id,
            'created_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testStatusFrontendOK()
    {
        $response = $this->get('/status');

        $response->assertStatus(200);
    }

    public function testStatusFrontendUseCacheForLastHourStatus()
    {
        Cache::spy();

        $response = $this->get('/status');

        $response->assertStatus(200);

        Cache::shouldHaveReceived('remember')
            ->with('last_hour_status', \Mockery::any(), \Mockery::any());
        Cache::shouldHaveReceived('remember')
            ->with('last_hour_status_created_at', \Mockery::any(), \Mockery::any());
    }

    public function testStatusFrontendUseCache()
    {
        Cache::spy();

        $response = $this->get('/status');

        $response->assertStatus(200);

        // cannot get this to work... TODO
        //Cache::shouldHaveReceived('put');
            //->with('check_type_' . $this->type->id . '_last_fail', \Mockery::any());
    }
}
