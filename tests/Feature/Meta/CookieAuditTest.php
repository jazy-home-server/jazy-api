<?php

namespace Tests\Feature\Meta;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CookieAuditTest extends TestCase
{
    use RefreshDatabase;
    public $gimyRoute = '/meta/gimycookie';
    public $checkRoute = '/meta/checkcookie';

    public function testGimmyCookie()
    {
        $cookie = [
            'cookie_name' => 'test',
            'cookie_value' => 'apple',
        ];

        $response = $this->withHeader('origin', 'testing')->postJson($this->gimyRoute, $cookie);

        $response->assertOk()
            ->assertJson($cookie)
            ->assertPlainCookie('test', 'apple');
        $this->assertDatabaseHas('meta_cookie_audits', ['cookies' => json_encode([['test' => 'apple']])]);
        $this->assertDatabaseHas('meta_cookie_audits', ['origin' => 'testing']);
    }

    public function testCheckCookie()
    {
        $cookie = [
            'test' => 'apple2',
        ];

        $response = $this->withCookies($cookie)->withHeader('origin', 'testing')->get($this->checkRoute);

        $response->assertOk()
            ->assertJsonStructure([['test']]);
        $this->assertDatabaseHas('meta_cookie_audits', ['origin' => 'testing']);
    }
}
