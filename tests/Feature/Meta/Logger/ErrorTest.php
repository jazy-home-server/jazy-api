<?php

namespace Tests\Feature\Meta\Logger;

use App\Models\Meta\Logger\Error;
use Tests\TestCase;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ErrorTest extends TestCase
{
    use RefreshDatabase;

    protected $route = '/meta/logger/errors';
    protected $errorStructure = [
        'id',
        'error',
        'extra',
        'error_date',
        'app',
        'origin',
        'created_at',
        'updated_at',
    ];

    public function testIndexErrors()
    {
        Error::factory()->count(5)->create();

        $response = $this->getJson($this->route);

        $response->assertOk()
            ->assertJsonStructure(['data', 'links']);
    }

    public function testPostingErrors()
    {
        $errorPayload = [
            'error' => json_encode(['name' => 'test1']),
            'extra' => json_encode(['testInfo' => 'apples']),
            'error_date' => Carbon::parse('15 minutes ago'),
            'app' => 'http://test.com',
        ];
        $response = $this->postJson($this->route, $errorPayload);
        $errorPayload['origin'] = 'Unknown';

        $response->assertCreated()
            ->assertJsonStructure(['data' => $this->errorStructure])
            ->assertJsonFragment($errorPayload);

        $resData = $response->decodeResponseJson()['data'];
        $this->assertDatabaseHas('meta_logger_errors', ['id' => $resData['id'], 'error' => $resData['error']]);
    }

    public function testPostingErrorsWithOrigin()
    {
        $errorPayload = [
            'error' => json_encode(['name' => 'test1']),
            'extra' => json_encode(['testInfo' => 'apples']),
            'error_date' => Carbon::parse('15 minutes ago'),
            'app' => 'http://test.com',
        ];
        $response = $this->withHeader('origin', 'http://notjazy.com')->postJson($this->route, $errorPayload);
        $errorPayload['origin'] = 'http://notjazy.com';

        $response->assertCreated()
            ->assertJsonStructure(['data' => $this->errorStructure])
            ->assertJsonFragment($errorPayload);

        $resData = $response->decodeResponseJson()['data'];
        $this->assertDatabaseHas('meta_logger_errors', ['id' => $resData['id'], 'error' => $resData['error']]);
    }

    public function testPostingErrorsWithISODate()
    {
        $errorPayload = [
            'error' => json_encode(['name' => 'test1']),
            'extra' => json_encode(['testInfo' => 'apples']),
            'error_date' => '2020-05-16T22:41:05.712000Z',
            'app' => 'http://test.com',
        ];
        $response = $this->postJson($this->route, $errorPayload);
        $errorPayload['origin'] = 'Unknown';

        $response->assertCreated()
            ->assertJsonStructure(['data' => $this->errorStructure])
            ->assertJsonFragment($errorPayload);

        $resData = $response->decodeResponseJson()['data'];
        $this->assertDatabaseHas('meta_logger_errors', ['id' => $resData['id'], 'error' => $resData['error']]);
    }

    public function testOptionsToErrorsCORSFromOwned()
    {
        $origin = 'http://www.jazyserver.com';
        $response = $this->withHeader('origin', $origin)->optionsJson($this->route);

        $response->assertOk()
            ->assertHeader('Access-Control-Allow-Origin', $origin);
    }

    public function testOptionsToErrorsCORSFromExternal()
    {
        $origin = 'http://www.notjazyserver.com';
        $response = $this->withHeader('origin', $origin)->optionsJson($this->route);

        $response->assertOk()
            ->assertHeader('Access-Control-Allow-Origin', '*');
    }
}
