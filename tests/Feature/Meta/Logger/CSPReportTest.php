<?php

namespace Tests\Feature\Meta\Logger;

use App\Models\Meta\Logger\CSPReport;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CSPReportTest extends TestCase
{
    use RefreshDatabase;
    // Per https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP
    protected $cspReportObject = [
        'document_uri',
        'referrer',
        'blocked_uri',
        'violated_directive',
        'original_policy',
    ];

    protected $route = '/meta/logger/csp-reports';

    public function testIndexCSPReport()
    {
        CSPReport::factory()->count(5)->create();

        $response = $this->getJson($this->route);

        $response->assertOk()
            ->assertJsonStructure(['data', 'links']);
    }

    public function testStoreCSPReportReportUri()
    {
        $payload = [
            'csp-report' => [
                'document-uri' => 'testuri',
                'referrer' => 'testreferrer',
                'blocked-uri' => 'testblockeduri',
                'violated-directive' => 'testdirective',
                'original-policy' => 'testpolicy',
            ]
        ];
        $response = $this
            ->withHeader('Content-Type', 'application/csp-report')
            ->postJson($this->route, $payload);

        $response->assertCreated()
            ->assertJsonFragment(['referrer' => 'testreferrer'])
            ->assertJsonStructure(['data' => $this->cspReportObject]);

        $this->assertDatabaseHas('meta_logger_csp_reports', ['document_uri' => 'testuri']);
    }

    public function testStoreCSPReportReportTo()
    {
        $payload = [
            'document-uri' => 'testuri',
            'referrer' => 'testreferrer',
            'blocked-uri' => 'testblockeduri',
            'violated-directive' => 'testdirective',
            'original-policy' => 'testpolicy',
        ];
        $response = $this
            ->withHeader('Content-Type', 'application/csp-report')
            ->postJson($this->route, $payload);

        $response->assertCreated()
            ->assertJsonFragment(['referrer' => 'testreferrer'])
            ->assertJsonStructure(['data' => $this->cspReportObject]);

        $this->assertDatabaseHas('meta_logger_csp_reports', ['document_uri' => 'testuri']);
    }
}
