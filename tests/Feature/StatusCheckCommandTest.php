<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\StatusCheck\CheckType;
use App\Models\StatusCheck\CheckError;
use App\Models\StatusCheck\StatusCheck;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Cache;
use Tests\Mocks\MockeryClientFactory;
use Illuminate\Support\Facades\Artisan;
use App\Contracts\HTTPClient\HTTPClient;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatusCheckCommandTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testStatusCheckCommandJobsQueued()
    {
        Queue::fake();

        Artisan::call('statuschecks:run');

        Queue::assertPushed(\App\Jobs\StatusCheck\CheckJazyServices::class);
    }

    /**
     * Test that the jobs are more or less working
     * (Actually tests that one specific check is completed)
     * Highly coupled to app/Jobs/StatusCheck/CheckJazyServices.php (jazyserver.com WebpageCheck)
     *
     * @return void
     */
    public function testStatusCheckJob()
    {
        $clientFactory = new MockeryClientFactory();
        $clientMock = $clientFactory->responseBody('Jazy Llerena')->responseStatusCode('200')->createClient();

        $this->instance(HTTPClient::class, $clientMock);

        Artisan::call('statuschecks:run');

        // Very coupled
        $this->assertDatabaseHas('check_types', ['name' => 'WebpageChecker', 'url' => 'https://jazyserver.com/']);
        $ct = CheckType::where('name', 'WebpageChecker')->where('url', 'https://jazyserver.com/')->first();


        $this->assertDatabaseHas('status_checks', ['check_type_id' => $ct->id]);
        $sc = StatusCheck::where('check_type_id', $ct->id)->orderBy('created_at', 'desc')->first();

        $this->assertTrue((bool)$sc->success, 'jazyserver.com was successful per test');
    }
}
