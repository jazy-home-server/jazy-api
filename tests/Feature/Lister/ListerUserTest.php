<?php

namespace Tests\Feature\Lister;

use App\Models\Lister\ListerUser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ListerUserTest extends TestCase
{
    use RefreshDatabase;

    public $listUserStructure = [ 'data' => [
        'id',
        'username',
        'created_at',
        'updated_at',
    ]];

    public $missing = [
        'email',
    ];

    public $route = '/test/lister/users';

    public function testIndexListerUsers()
    {
        ListerUser::factory()->count(5)->create();
        $response = $this->get($this->route);

        $indexStructure = [
            'data' => [$this->listUserStructure['data']],
            'links',
            'meta'
        ];

        $response->assertStatus(200)
            ->assertJsonStructure($indexStructure)
            ->assertJsonMissing($this->missing);
    }

    public function testShowListerUser()
    {
        $username = 'apple';
        ListerUser::factory()->count(5)->create();
        ListerUser::factory()->create([
            'username' => $username,
        ]);

        $response = $this->get($this->route . "/$username");

        $response->assertOk()
            ->assertJsonStructure($this->listUserStructure)
            ->assertJsonFragment(['username' => $username])
            ->assertJsonMissing($this->missing);
    }

    public function testShowListerUserWithEmail()
    {
        ListerUser::factory()->count(5)->create();
        $user = ListerUser::factory()->create([
            'username' => 'apple',
            'email' => 'app@emample.com',
        ]);

        $response = $this->get($this->route . "/$user->username");

        $response->assertOk()
            ->assertJsonStructure($this->listUserStructure)
            ->assertJsonFragment([
                'username' => $user->username,
            ])
            ->assertJsonMissing($this->missing);
    }

    public function testCreateListerUser()
    {
        $user = [
            'username' => 'appletree',
        ];

        $response = $this->post($this->route, $user);

        $response->assertCreated()
            ->assertJsonStructure($this->listUserStructure)
            ->assertJsonFragment([
                'username' => $user['username'],
            ])
            ->assertJsonMissing($this->missing);
        $this->assertDatabaseHas('lister_users', $user);
    }

    public function testCreateListerUserWithEmail()
    {
        $user = [
            'username' => 'appletree',
            'email' => 'ajaj@example.com',
        ];

        $response = $this->post($this->route, $user);
        $response->assertCreated()
            ->assertJsonStructure($this->listUserStructure)
            ->assertJsonFragment([
                'username' => $user['username'],
            ])
            ->assertJsonMissing($this->missing);
        $this->assertDatabaseHas('lister_users', $user);
    }

    // Updating of username disabled due to lack of auth
    /*
    public function testUpdateListerUser()
    {
        $user = ListerUser::factory()->create([
            'username' => 'apple',
            'email' => 'app@emample.com',
            'updated_at' => \Carbon\Carbon::parse('5 minutes ago'),
        ]);

        $payload = [
            'username' => 'tree',
        ];

        $response = $this->patch($this->route . "/$user->username", $payload);

        $response->assertOk()
            ->assertJsonStructure($this->listUserStructure)
            ->assertJsonFragment([
                'username' => $payload['username'],
            ])
            ->assertJsonMissing($this->missing)
            ->assertJsonMissing(['username' => $user->username])
            ->assertJsonMissing(['updated_at' => $user->updated_at]);
        $this->assertDatabaseHas('lister_users', [
            'email' => $user->email,
            'username' => $payload['username'],
        ]);
        $this->assertDatabaseMissing('lister_users', [
            'username' => $user->username,
        ]);
    }
    */

    public function testUpdateListerUserSilentFail()
    {
        $user = ListerUser::factory()->create([
            'username' => 'apple',
            'email' => 'app@emample.com',
            'updated_at' => \Carbon\Carbon::parse('5 minutes ago'),
        ]);

        $payload = [
            'username' => 'tree',
        ];

        $response = $this->patch($this->route . "/$user->username", $payload);

        $response->assertOk()
            ->assertJsonStructure($this->listUserStructure)
            ->assertJsonFragment([
                'username' => $user->username, // swapped
            ])
            ->assertJsonMissing($this->missing)
            ->assertJsonMissing([
                'username' => $payload['username'], // swapped
            ])
            ->assertJsonFragment(['updated_at' => $user->updated_at]);
        $this->assertDatabaseHas('lister_users', [
            'email' => $user->email,
            'username' => $user->username,
        ]);
        $this->assertDatabaseMissing('lister_users', [
            'username' => $payload['username'],
        ]);
    }

    public function testUpdateListerUserEmail()
    {
        $user = ListerUser::factory()->create([
            'username' => 'apple',
            'email' => 'app@emample.com',
            'updated_at' => \Carbon\Carbon::parse('5 minutes ago'),
        ]);

        $payload = [
            'email' => 'asd@asd.com',
        ];

        $response = $this->patch($this->route . "/$user->username", $payload);

        $response->assertOk()
            ->assertJsonStructure($this->listUserStructure)
            ->assertJsonFragment([
                'username' => $user->username,
            ])
            ->assertJsonMissing($this->missing)
            ->assertJsonMissing(['email' => $user->username])
            ->assertJsonMissing(['updated_at' => $user->updated_at]);
        $this->assertDatabaseHas('lister_users', [
            'username' => $user->username,
            'email' => $payload['email'],
        ]);
    }

    public function testDestroyListerUser()
    {
        $user = ListerUser::factory()->create([
            'username' => 'apple',
            'email' => 'app@emample.com',
        ]);

        $response = $this->delete($this->route . "/$user->username");

        // Disabled destroy for meantime, test that it fails
        return $response->assertStatus(405);

        $response->assertUnauthorized()
            ->assertExactJson([]);
        $this->assertDatabaseHas('lister_users', ['username' => $user->username]);
    }
}
