<?php

namespace Tests\Feature\Lister;

use Tests\TestCase;
use App\Models\Lister\ACList;
use App\Models\Lister\ACSong;
use App\Models\Lister\ACListSong;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ACListSongTest extends TestCase
{
    use RefreshDatabase;

    public $acListSongStructure = [ 'data' => [
        'id',
        'list_id',
        'song_id',
        'created_at',
        'updated_at',
    ]];

    public $route = '/test/lister/acmusic/listsongs';

    public function testACListSongIndex()
    {
        ACListSong::factory()->create();
        $indexStructure = [
            'data' => [$this->acListSongStructure['data']],
            'links',
            'meta'
        ];

        $response = $this->getJson($this->route);

        $response->assertStatus(200)
            ->assertJsonStructure($indexStructure);
    }

    public function testACListSongIndexPaginationCount()
    {
        ACListSong::factory()->count(10)->create();
        $indexStructure = [
            'data' => [$this->acListSongStructure['data']],
            'links',
            'meta'
        ];

        $response = $this->getJson($this->route . '?pag_count=1');

        $response->assertStatus(200)
            ->assertJsonStructure($indexStructure)
            ->assertJsonCount(1, 'data');
    }

    public function testACListSongIndexExpandSong()
    {
        $listSong = ACListSong::factory()->create();
        $indexStructure = [
            'data' => [$this->acListSongStructure['data']],
            'links',
            'meta'
        ];
        $dataFrag = [
            'song' => [
                'id' => $listSong->song->id,
                'name' => $listSong->song->name,
                'img_url' => $listSong->song->img_url,
            ],
        ];

        $response = $this->getJson($this->route . '?expand=song');

        $response->assertStatus(200)
            ->assertJsonFragment($dataFrag)
            ->assertJsonStructure($indexStructure);
    }

    public function testACListSongIndexSearchByList()
    {
        ACListSong::factory()->count(5)->create();
        $list = ACList::factory()->create();
        $listSongs = ACListSong::factory()->count(5)->create(['list_id' => $list->id]);
        $indexStructure = [
            'data' => [$this->acListSongStructure['data']],
            'links',
            'meta'
        ];

        $response = $this->getJson($this->route . "?list_id={$list->id}");

        $response->assertStatus(200)
            ->assertJsonStructure($indexStructure)
            ->assertJsonFragment(['list_id' => $list->id . ''])
            ->assertJsonCount(5, 'data');
    }

    public function testACListSongShow()
    {
        $listSong = ACListSong::factory()->create();

        $response = $this->getJson($this->route . "/{$listSong->id}");

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $listSong->id,
                'list_id' => $listSong->list_id . '',
                'song_id' => $listSong->song_id . '',
                'checked' => $listSong->checked . '',
            ])
            ->assertJsonStructure($this->acListSongStructure);
    }

    public function testACListSongShowExpandSong()
    {
        $listSong = ACListSong::factory()->create();

        $songData = [
            'id' => $listSong->song->id,
            'name' => $listSong->song->name,
            'img_url' => $listSong->song->img_url,
        ];

        $response = $this->getJson($this->route . '?expand=song');

        $response->assertStatus(200)
            ->assertJsonFragment($songData);
    }

    public function testACListSongCreate()
    {
        $list = ACList::factory()->create();
        $song = ACSong::factory()->create();
        $listSong = [
            'checked' => 0,
            'list_id' => $list->id,
            'song_id' => $song->id,
        ];

        $response = $this->postJson($this->route, $listSong);

        $response->assertCreated()
            ->assertJsonStructure($this->acListSongStructure)
            ->assertJsonFragment($listSong);

        $this->assertDatabaseHas('lister_ac_list_song', [
            'id' => $response->decodeResponseJson()['data']['id'],
        ]);
    }

    public function testACListSongCreateFailDuplicate()
    {
        $list = ACList::factory()->create();
        $song = ACSong::factory()->create();
        $listSong = [
            'checked' => 0,
            'list_id' => $list->id,
            'song_id' => $song->id,
        ];
        $firstListSong = ACListSong::factory()->create($listSong);

        $listSong['checked'] = 1;
        $response = $this->postJson($this->route, $listSong);

        $response->assertStatus(422);

        $this->assertDatabaseHas('lister_ac_list_song', [
            'id' => $firstListSong->id,
        ]);
        $this->assertDatabaseMissing('lister_ac_list_song', [
            'checked' => $listSong['checked'],
        ]);
    }

    public function testACListSongUpdate()
    {
        $pin = '1234';
        $list = ACList::factory()->create([ 'pin' => Hash::make($pin) ]);
        $song = ACSong::factory()->create();
        $listSong = ACListSong::factory()->create([
            'checked' => 0,
            'list_id' => $list->id,
            'song_id' => $song->id,
            'updated_at' => \Carbon\Carbon::parse('5 minutes ago'),
        ]);

        $newData = [
            'pin' => $pin,
            'checked' => 1,
        ];

        $response = $this->patchJson($this->route . "/{$listSong->id}", $newData);

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $listSong->id,
                'list_id' => $listSong->id . '',
            ])
            ->assertJsonFragment([ 'checked' => $newData['checked'] ])
            ->assertJsonMissing([
                'updated_at' => $listSong->updated_at,
            ])
            ->assertJsonStructure($this->acListSongStructure);

        $listSong->refresh();

        $this->assertEquals($newData['checked'], $listSong->checked, 'Checked was not updated');
    }

    public function testACListSongUpdateFailWithInvalidPin()
    {
        $list = ACList::factory()->create([
            'pin' => Hash::make('4321'),
        ]);
        $song = ACSong::factory()->create();
        $listSong = ACListSong::factory()->create([
            'checked' => 0,
            'list_id' => $list->id,
            'song_id' => $song->id,
            'updated_at' => \Carbon\Carbon::parse('5 minutes ago'),
        ]);

        $newData = [
            'pin' => '1234',
            'checked' => 1,
        ];

        $response = $this->patchJson($this->route . "/{$listSong->id}", $newData);

        $response->assertUnauthorized();

        $listSong->refresh();

        $this->assertEquals($listSong->checked, $listSong->checked, 'Checked was updated');
    }

    public function testACListSongUpdateFailChangeIds()
    {
        $pin = '1234';
        $list = ACList::factory()->create([ 'pin' => Hash::make($pin) ]);
        $song = ACSong::factory()->create();
        $listSong = ACListSong::factory()->create([
            'checked' => 0,
            'list_id' => $list->id,
            'song_id' => $song->id,
            'updated_at' => \Carbon\Carbon::parse('5 minutes ago'),
        ]);

        $newData = [
            'pin' => $pin,
            'checked' => 1,
            'list_id' => '1234',
            'song_id' => $song->id,
        ];

        $response = $this->patchJson($this->route . "/{$listSong->id}", $newData);

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $listSong->id,
                'list_id' => $listSong->list_id . '',
                'song_id' => $listSong->song_id . '',
            ])
            ->assertJsonFragment(['checked' => $newData['checked']])
            ->assertJsonMissing([ // Updated checked
                'updated_at' => $listSong->updated_at,
            ])
            ->assertJsonStructure($this->acListSongStructure);
    }

    public function testACListSongUpdateFailUpdatingWhenNoChange()
    {
        $pin = '1234';
        $list = ACList::factory()->create([ 'pin' => Hash::make($pin) ]);
        $song = ACSong::factory()->create();
        $listSong = ACListSong::factory()->create([
            'checked' => 0,
            'list_id' => $list->id,
            'song_id' => $song->id,
            'updated_at' => \Carbon\Carbon::parse('5 minutes ago'),
        ]);

        $newData = [
            'pin' => $pin,
            'checked' => '0',
            'list_id' => '1234',
            'song_id' => $song->id,
        ];

        $response = $this->patchJson($this->route . "/{$listSong->id}", $newData);

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $listSong->id,
                'list_id' => $listSong->list_id . '',
                'song_id' => $listSong->song_id . '',
            ])
            ->assertJsonFragment(['checked' => $newData['checked']])
            ->assertJsonFragment([
                'updated_at' => $listSong->updated_at,
            ])
            ->assertJsonStructure($this->acListSongStructure);
    }

    public function testACListSongDestroy()
    {
        $listSong = ACListSong::factory()->create();

        $response = $this->deleteJson($this->route . "/{$listSong->id}");

        // Disabled destroy for meantime, test that it fails
        return $response->assertStatus(405);

        $response->assertOk()->assertJson([]);
        $this->assertDatabaseMissing('lister_ac_list_song', [
            'id' => $listSong->id,
        ]);
    }
}
