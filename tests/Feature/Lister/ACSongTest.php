<?php

namespace Tests\Feature\Lister;

use Tests\TestCase;
use App\Models\Lister\ACSong;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ACSongTest extends TestCase
{
    use RefreshDatabase;

    public $acSongStructure = [ 'data' => [
        'id',
        'name',
        'img_url',
    ]];

    public $route = '/test/lister/acmusic/songs';

    public function testACSongIndex()
    {
        ACSong::factory()->create();
        $indexStructure = [
            'data' => [$this->acSongStructure['data']],
        ];

        $response = $this->get($this->route);

        $response->assertStatus(200)
            ->assertJsonStructure($indexStructure)
            ->assertJsonMissing(['links', 'meta']);
    }

    public function testACSongShow()
    {
        $song = ACSong::factory()->create();

        $response = $this->get($this->route . "/{$song->id}");

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $song->id,
                'img_url' => $song->img_url,
                'name' => $song->name,
            ])
            ->assertJsonStructure($this->acSongStructure);
    }

    public function testACSongNoUpdate()
    {
        $song = ACSong::factory()->create();

        $response = $this->patch($this->route . "/{$song->id}", [
            'name' => 'apple',
        ]);
        $response->assertStatus(405);
    }

    public function testACSongNoPost()
    {
        $song = ACSong::factory()->create();

        $response = $this->postJson($this->route, [ 'name' => 'test', 'img_url' => 'test' ]);
        $response->assertStatus(405);
    }

    public function testACSongNoDelete()
    {
        $song = ACSong::factory()->create();

        $response = $this->delete($this->route . "/{$song->id}");
        $response->assertStatus(405);
    }
    /* Disabled all but the Index route */
    public function testACSongCreate()
    {
        $songData = [
            'name' => 'iloveapples',
            'img_url' => 'http://www.jazyserver.com',
        ];

        $response = $this->postJson($this->route, $songData);

        // Disabled
        return $response->assertStatus(405);

        $response->assertCreated()
            ->assertJsonStructure($this->acSongStructure)
            ->assertJsonFragment(['name' => $songData['name']]);

        $this->assertDatabaseHas('lister_ac_song', [
            'id' => $response->decodeResponseJson()['data']['id'],
        ]);
    }

    public function testACSongUpdate()
    {
        $name = 'apples';
        $newName = 'iloveapples';
        $song = ACSong::factory()->create([
            'name' => $name,
        ]);

        $response = $this->patch($this->route . "/{$song->id}", [
            'name' => $newName,
        ]);

        // Disabled
        return $response->assertStatus(405);

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $song->id,
                'name' => $newName,
            ])
            ->assertJsonMissing([
                'updated_at' => $song->updated_at,
            ])
            ->assertJsonStructure($this->acSongStructure);

        $song->refresh();

        $this->assertEquals($newName, $song->name, 'Name was not changed');
    }

    public function testACSongDestroy()
    {
        $song = ACSong::factory()->create();

        $response = $this->delete($this->route . "/{$song->id}");

        // Disabled
        return $response->assertStatus(405);

        $response->assertOk()->assertJson([]);
        $this->assertDatabaseMissing('lister_ac_song', [
            'id' => $song->id,
        ]);
    }
}
