<?php

namespace Tests\Feature\Lister;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\Lister\ACList;
use App\Models\Lister\ACSong;
use App\Models\Lister\ACListSong;
use App\Models\Lister\ListerUser;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ACListTest extends TestCase
{
    use RefreshDatabase;

    public $acListStructure = [ 'data' => [
        'id',
        'user_id',
        'created_at',
        'updated_at',
    ]];

    public $route = '/test/lister/acmusic/lists';

    public function testACListIndex()
    {
        ACList::factory()->create();
        $indexStructure = [
            'data' => [$this->acListStructure['data']],
            'links',
            'meta'
        ];

        $response = $this->getJson($this->route);

        $response->assertStatus(200)
            ->assertJsonStructure($indexStructure)
            ->assertJsonMissing(['pin']);
    }

    public function testACListIndexFilterByUser()
    {
        ACList::factory()->count(10)->create();
        $user = ListerUser::factory()->create();
        ACList::factory()->create(['user_id' => $user->id]);
        $indexStructure = [
            'data' => [$this->acListStructure['data']],
            'links',
            'meta'
        ];

        $response = $this->getJson($this->route . '?user_id=' . $user->id);

        $response->assertStatus(200)
            ->assertJsonStructure($indexStructure)
            ->assertJsonFragment(['user_id' => $user->id . ''])
            ->assertJsonCount(1, ['data'])
            ->assertJsonMissing(['pin']);
    }

    public function testACListIndexFilterByUserFailUserNoLists()
    {
        ListerUser::factory()->count(10)->create();
        $user = ListerUser::factory()->create();

        $response = $this->getJson($this->route . '?user_id=' . $user->id);

        $response->assertNotFound()
            ->assertJsonStructure(['message']);
    }

    public function testACListIndexFilterByUserFailUserNotFound()
    {
        $response = $this->getJson($this->route . '?user_id=' . 1);

        $response->assertNotFound()
            ->assertJsonStructure(['message']);
    }

    public function testACListShow()
    {
        $list = ACList::factory()->create();

        $response = $this->getJson($this->route . "/{$list->id}");

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $list->id,
                'user_id' => $list->user_id . '',
            ])
            ->assertJsonStructure($this->acListStructure)
            ->assertJsonMissing(['pin']);
    }

    public function testACListAuth()
    {
        $pin = '1234';
        $list = ACList::factory()->create([
            'pin' => Hash::make($pin),
        ]);

        $response = $this->postJson($this->route . "/{$list->id}/auth", ['pin' => $pin]);

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $list->id,
                'user_id' => $list->user_id . '',
            ])
            ->assertJsonStructure($this->acListStructure)
            ->assertJsonMissing(['pin']);
    }

    public function testACListFailAuth()
    {
        $pin = '1234';
        $list = ACList::factory()->create([
            'pin' => Hash::make($pin),
        ]);

        $response = $this->postJson($this->route . "/{$list->id}/auth", ['pin' => '1111']);

        $response->assertUnauthorized()
            ->assertJsonMissing([
                'id' => $list->id,
                'user_id' => $list->user_id . '',
            ])
            ->assertJsonStructure([
                'message'
            ])
            ->assertJsonMissing(['pin']);
    }

    public function testACListCreate()
    {
        $user = ListerUser::factory()->create();
        $list = [
            'pin' => '2131',
            'user_id' => $user->id,
        ];

        $response = $this->postJson($this->route, $list);

        $response->assertCreated()
            ->assertJsonStructure($this->acListStructure)
            ->assertJsonFragment(['user_id' => $user->id])
            ->assertJsonMissing(['pin']);

        $this->assertDatabaseHas('lister_ac_list', [
            'user_id' => $user->id,
            'id' => $response->decodeResponseJson()['data']['id']
        ]);
    }

    public function testACListFailCreateMultipleOneUser()
    {
        $user = ListerUser::factory()->create();
        ACList::factory()->create(['user_id' => $user->id]);

        $list = [
            'pin' => '2131',
            'user_id' => $user->id,
        ];

        $response = $this->postJson($this->route, $list);

        $response->assertStatus(422)
            ->assertJsonFragment([['User already has a list']])
            ->assertJsonStructure(['message', 'errors'])
            ->assertJsonMissing(['pin']);

        $this->assertEquals(
            1,
            ACList::query()->where('user_id', '=', $user->id)->get()->count(),
        'More than 1 list per user exists');
    }

    public function testACListCreateAndPopulateSongList()
    {
        $user = ListerUser::factory()->create();
        $list = [
            'pin' => '2131',
            'user_id' => $user->id,
        ];

        $response = $this->postJson($this->route, $list);

        $response->assertCreated()
            ->assertJsonStructure($this->acListStructure)
            ->assertJsonFragment(['user_id' => $user->id])
            ->assertJsonMissing(['pin']);

        $this->assertDatabaseHas('lister_ac_list', [
            'user_id' => $user->id,
            'id' => $response->decodeResponseJson()['data']['id']
        ]);

        $this->assertDatabaseHas('lister_ac_list_song', [
            'list_id' => $response->json('data')['id'],
        ]);

        $this->assertEquals(ACSong::all()->count(), ACListSong::all()->count());
    }

    public function testACListUpdate()
    {
        $pin = '1122';
        $newPin = '2134';
        $list = ACList::factory()->create([
            'pin' => Hash::make($pin),
            'updated_at' => Carbon::parse('5 minutes ago'),
        ]);

        $response = $this->patch($this->route . "/{$list->id}", [
            'oldpin' => $pin,
            'newpin' => $newPin,
        ]);

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $list->id,
                'user_id' => $list->user_id . '',
            ])
            ->assertJsonMissing([
                'updated_at' => $list->updated_at,
            ])
            ->assertJsonStructure($this->acListStructure)
            ->assertJsonMissing(['pin']);

        $list->refresh();

        $this->assertTrue(Hash::check($newPin, $list->pin), 'Pin was not changed');
    }

    public function testACListUpdateFailChangeIds()
    {
        $pin = '1122';
        $newPin = '2134';
        $list = ACList::factory()->create([
            'pin' => Hash::make($pin),
            'updated_at' => Carbon::parse('5 minutes ago'),
        ]);

        $response = $this->patch($this->route . "/{$list->id}", [
            'oldpin' => $pin,
            'newpin' => $newPin,
            'user_id' => '1234',
        ]);

        $response->assertOk()
            ->assertJsonFragment([
                'id' => $list->id,
                'user_id' => $list->user_id . '',
            ])
            ->assertJsonMissing([ // Pins were updated
                'updated_at' => $list->updated_at,
            ])
            ->assertJsonStructure($this->acListStructure)
            ->assertJsonMissing(['pin']);
    }

    public function testACListFailUpdateInvalidPin()
    {
        $pin = '1122';
        $newPin = '2134';
        $list = ACList::factory()->create([
            'pin' => Hash::make($pin),
            'updated_at' => Carbon::parse('5 minutes ago'),
        ]);

        $response = $this->patch($this->route . "/{$list->id}", [
            'oldpin' => '1231',
            'newpin' => $newPin,
        ]);

        $response->assertUnauthorized()
            ->assertJsonMissing([
                'updated_at' => $list->updated_at,
                'pin',
            ]);

        $list->refresh();

        $this->assertTrue(Hash::check($pin, $list->pin), 'Pin has changed');
    }

    public function testACListDestroy()
    {
        $pin = '2211';
        $list = ACList::factory()->create([
            'pin' => Hash::make($pin),
        ]);

        $response = $this->delete($this->route . "/{$list->id}", [
            'pin' => $pin,
        ]);

        // Disabled destroy for meantime, test that it fails
        return $response->assertStatus(405);

        $response->assertOk()->assertJson([]);
        $this->assertDatabaseMissing('lister_ac_list', [
            'id' => $list->id,
        ]);
    }
}
