<?php

namespace Tests\Feature;

use Carbon\Carbon;
use Tests\TestCase;
use App\Models\Note;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NoteTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testNoteIndex()
    {
        Note::factory()->count(5)->create();

        $indexStructure = [
            'data' => [
                [
                    'id',
                    'title',
                    'text',
                    'created_at',
                    'updated_at',
                ]
            ],
            'links',
            'meta',
        ];
        $response = $this->json('GET', '/test/notes');

        $response->assertOk()
            ->assertJsonStructure($indexStructure);
    }

    public function testNoteShow()
    {
        Note::factory()->count(5)->create();

        $showStructure = [
            'id',
            'title',
            'text',
            'created_at',
            'updated_at',
        ];
        $response = $this->json('GET', '/test/notes/1');

        $response->assertOk()
            ->assertJsonStructure(['data' => $showStructure]);
    }

    public function testNotePost()
    {
        Note::factory()->count(5)->create();

        $payload = [
            'title' => 'test',
            'text' => 'hello world',
        ];
        $postStructure = [
            'id',
            'title',
            'text',
            'created_at',
            'updated_at',
        ];
        $response = $this->json('POST', '/test/notes/', $payload);

        $response->assertCreated()
            ->assertJsonStructure(['data' => $postStructure])
            ->assertJson(['data' => $payload]);
    }

    public function testNoteUpdate()
    {
        Note::factory()->count(5)->create([
            'updated_at' => Carbon::parse('5 minutes ago'),
        ]);
        $victimNote = Note::find(1);

        $payload = [
            'title' => 'hello apples',
        ];
        $putStructure = [
            'id',
            'title',
            'text',
            'created_at',
            'updated_at',
        ];

        $response = $this->json('PUT', '/test/notes/1', $payload);

        $response->assertOk()
            ->assertJsonStructure(['data' => $putStructure])
            ->assertJson(['data' => $payload])
            ->assertJsonFragment([
                'text' => $victimNote->text,
                'created_at' => $victimNote->created_at,
            ])
            ->assertJsonMissing(['data' =>  [
                'updated_at' => $victimNote->updated_at,
            ]]);
    }

    public function testNoteDestroy()
    {
        Note::factory()->count(5)->create();

        $response = $this->json('DELETE', '/test/notes/1');

        $response->assertOk();
    }

    public function testNoteShowNotFound()
    {
        Note::factory()->count(5)->create();

        $response = $this->json('GET', '/test/notes/10000');

        $response->assertNotFound();
    }

    public function testNoteUpdateNotFound()
    {
        Note::factory()->count(5)->create();

        $response = $this->json('PUT', '/test/notes/10000', []);

        $response->assertNotFound();
    }

    public function testNoteDestroyNotFound()
    {
        Note::factory()->count(5)->create();

        $response = $this->json('DELETE', '/test/notes/10000');

        $response->assertNotFound();
    }

}
