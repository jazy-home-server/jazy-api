<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Support\Carbon;
use App\Models\StatusCheck\CheckType;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Queue;
use Tests\Mocks\MockeryClientFactory;
use App\Models\StatusCheck\CheckError;
use App\Models\StatusCheck\StatusCheck;
use Illuminate\Support\Facades\Artisan;
use App\Contracts\HTTPClient\HTTPClient;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatusCheckCacheCommandTest extends TestCase
{
    use RefreshDatabase;

    public function testStatusCheckCacheCommand()
    {
        $this->artisan('statuschecks:cache')->assertExitCode(0);
    }

    /**
     *
     * @return void
     */
    public function testStatusCheckCacheCommandCacheLastHourStatus()
    {
        $this->artisan('statuschecks:cache');
        $this->assertNotNull(Cache::get('last_hour_status'));
    }

    /**
     *
     * @return void
     */
    public function testStatusCheckCacheCommandCacheLastHourStatusCreatedAt()
    {
        $this->artisan('statuschecks:cache');
        $this->assertNotNull(Cache::get('last_hour_status_created_at'));
    }

    /**
     *
     * @return void
     */
    public function testStatusCheckCacheCommandCacheLastTypeSuccess()
    {
        $type = CheckType::factory()->create();
        StatusCheck::factory()->count(5)->create(['check_type_id' => $type->id, 'success' => '1',
            'created_at' => Carbon::parse('30 minutes ago')->toDateTimeString(),
        ]);
        $statusCheck = StatusCheck::factory()->create(['check_type_id' => $type->id, 'success' => '1',
            'created_at' => Carbon::parse('now')->toDateTimeString(),
        ]);
        $this->artisan('statuschecks:cache');

        $lastSuccess = Cache::get('check_type_' . $type->id . '_last_success');
        $this->assertNotNull(Cache::get('check_type_' . $type->id . '_last_success'));
        $this->assertNull(Cache::get('check_type_' . $type->id . '_last_fail'));
        $this->assertEquals($statusCheck->id, $lastSuccess->id);
    }

    /**
     *
     * @return void
     */
    public function testStatusCheckCacheCommandCacheLastTypeFail()
    {
        $type = CheckType::factory()->create();
        StatusCheck::factory()->count(5)->create(['check_type_id' => $type->id, 'success' => '0',
            'created_at' => Carbon::parse('30 minutes ago')->toDateTimeString(),
        ]);
        $statusCheck = StatusCheck::factory()->create(['check_type_id' => $type->id, 'success' => '0',
            'created_at' => Carbon::parse('now')->toDateTimeString(),
        ]);

        $this->artisan('statuschecks:cache');

        $lastFail = Cache::get('check_type_' . $type->id . '_last_fail');
        $this->assertNotNull($lastFail);
        $this->assertNull(Cache::get('check_type_' . $type->id . '_last_success'));
        $this->assertEquals($statusCheck->id, $lastFail->id);
    }

    public function testStatusCheckCacheCommandUseCache()
    {
        // TODO check that no type cache is hit
        $this->artisan('statuschecks:cache')->assertExitCode(0);
        $this->assertNotNull(Cache::get('last_hour_status_created_at'));
        $lastHourStatusCreatedAt = Cache::get('last_hour_status_created_at');

        $this->artisan('statuschecks:cache');
        $this->assertEquals($lastHourStatusCreatedAt, Cache::get('last_hour_status_created_at'));

    }

    public function testStatusCheckCacheCommandCacheLastFailSinceId()
    {
        $type = CheckType::factory()->create();
        StatusCheck::factory()->count(5)->create(['check_type_id' => $type->id, 'success' => '0',
            'created_at' => Carbon::parse('30 minutes ago')->toDateTimeString(),
        ]);
        $statusCheck = StatusCheck::factory()->create(['check_type_id' => $type->id, 'success' => '0',
            'created_at' => Carbon::parse('now')->toDateTimeString(),
        ]);

        $this->artisan('statuschecks:cache');

        $this->assertEquals($statusCheck->id, Cache::get('check_type_' . $type->id . '_last_fail_since_id'));
        $this->assertEquals($statusCheck->id, Cache::get('check_type_' . $type->id . '_last_success_since_id'));
    }

    public function testStatusCheckCacheCommandCacheLastSuccessSinceId()
    {
        $type = CheckType::factory()->create();
        StatusCheck::factory()->count(5)->create(['check_type_id' => $type->id, 'success' => '1',
            'created_at' => Carbon::parse('30 minutes ago')->toDateTimeString(),
        ]);
        $statusCheck = StatusCheck::factory()->create(['check_type_id' => $type->id, 'success' => '1',
            'created_at' => Carbon::parse('now')->toDateTimeString(),
        ]);

        $this->artisan('statuschecks:cache');

        $this->assertEquals($statusCheck->id, Cache::get('check_type_' . $type->id . '_last_fail_since_id'));
        $this->assertEquals($statusCheck->id, Cache::get('check_type_' . $type->id . '_last_success_since_id'));
    }
}
