<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CORSHeadersTest extends TestCase
{
    public function testOptionsOnTestRoute()
    {
        $origin = 'http://www.jazyserver.com';
        $response = $this->withHeader('origin', $origin)->optionsJson('/test/notes');

        $response->assertOk()
            ->assertHeader('Access-Control-Allow-Origin', $origin);
    }

    public function testOptionsOnTestRouteFail()
    {
        $origin = 'http://www.notjazyserver.com';
        $response = $this->withHeader('origin', $origin)->optionsJson('/test/notes');

        $response->assertOk()
            ->assertHeaderMissing('Access-Control-Allow-Origin');
    }

    public function testOptionsOnTestRouteOnLocal()
    {
        config(['app.env' => 'local']);
        $origin = 'http://www.notjazyserver.com';
        $response = $this->withHeader('origin', $origin)->optionsJson('/test/notes');

        $response->assertOk()
            ->assertHeader('Access-Control-Allow-Origin', '*');
    }
}
