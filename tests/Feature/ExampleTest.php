<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertOk();
    }

    public function testHomepageAppName()
    {
        config(['app.name' => 'testapp']);

        $response = $this->get('/');

        $response->assertOk()
            ->assertSee('testapp');
    }
}
