<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\FakeUser;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class FakeUserTest extends TestCase
{
    use RefreshDatabase;

    public $fakeUserStructure = [
        'id',
        'first_name',
        'last_name',
        'email',
        'created_at',
        'updated_at',
    ];

    /**
     * Test the user(s) index
     *
     * @return void
     */
    public function testFakeUserIndex()
    {
        FakeUser::factory()->count(5)->create();

        $indexStructure = [
            'data' => [
                $this->fakeUserStructure,
            ],
            'links',
            'meta',
        ];
        $response = $this->json('GET', '/test/users');

        $response->assertOk()
            ->assertJsonStructure($indexStructure)
            ->assertJsonMissing(['password', 'user_token']);
    }

    public function testFakeUserShow()
    {
        FakeUser::factory()->count(5)->create();

        $response = $this->json('GET', '/test/users/1');

        $response->assertOk()
            ->assertJsonStructure(['data' => $this->fakeUserStructure]);
    }

    public function testFakeUserPost()
    {
        FakeUser::factory()->count(5)->create();

        $payload = [
            'email' => 'test@example.com',
            'password' => 'helloworld',
            'first_name' => 'test',
            'last_name' => 'apples',
        ];
        $response = $this->json('POST', '/test/users/', $payload);

        $response->assertCreated()
            ->assertJsonStructure(['data' => $this->fakeUserStructure])
            ->assertJson(['data' => Arr::except($payload, 'password')]);

        // Check that the password was indeed hashed
        $responseData = $response->json()['data'];
        $fakeUserStoredPassword = FakeUser::find($responseData['id'])->password;
        $this->assertTrue(Hash::check($payload['password'], $fakeUserStoredPassword), 'Fake User password was not hashed');
    }

    public function testFakeUserUpdate()
    {
        FakeUser::factory()->count(5)->create();
        $victimFakeUser = FakeUser::find(1);

        $payload = [
            'first_name' => 'hello_apples',
        ];

        $response = $this->json('PUT', '/test/users/1', $payload);

        $response->assertOk()
            ->assertJsonStructure(['data' => $this->fakeUserStructure])
            ->assertJson(['data' => $payload])
            ->assertJson(['data' => [
                'last_name' => $victimFakeUser->last_name,
                'created_at' => $victimFakeUser->created_at->toJson(),
            ]])
            ->assertJsonMissing(['data' =>  [
                'password',
                'updated_at' => $victimFakeUser->updated_at,
            ]]);
    }

    public function testFakeUserUpdatePassword()
    {
        // TODO remove/replace this functionality
        FakeUser::factory()->count(5)->create();
        $victimFakeUser = FakeUser::find(1);

        $payload = [
            'password' => 'hello_apples',
        ];

        $response = $this->json('PUT', '/test/users/1', $payload);

        $response->assertOk()
            ->assertJsonStructure(['data' => $this->fakeUserStructure])
            ->assertJson([ 'data' => [
                'last_name' => $victimFakeUser->last_name,
                'created_at' => $victimFakeUser->created_at->toJson(),
            ]])
            ->assertJsonMissing(['data' =>  [
                'password',
                'updated_at' => $victimFakeUser->updated_at,
            ]]);
    }

    public function testFakeUserDestroy()
    {
        FakeUser::factory()->count(5)->create();

        $response = $this->json('DELETE', '/test/users/1');

        $response->assertOk();

        // Check that the user was indeed deleted
        $this->assertNull(FakeUser::find(1), 'Fake User was not actually deleted from the DB');
    }

    public function testFakeUserShowNotFound()
    {
        FakeUser::factory()->count(5)->create();

        $response = $this->json('GET', '/test/users/10000');

        $response->assertNotFound();
    }

    public function testFakeUserUpdateNotFound()
    {
        FakeUser::factory()->count(5)->create();

        $response = $this->json('PUT', '/test/users/10000', []);

        $response->assertNotFound();
    }

    public function testFakeUserDestroyNotFound()
    {
        FakeUser::factory()->count(5)->create();

        $response = $this->json('DELETE', '/test/users/10000');

        $response->assertNotFound();
    }

}
