<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\StatusCheck\CheckType;
use App\Models\StatusCheck\CheckError;
use App\Models\StatusCheck\StatusCheck;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatusCheckTest extends TestCase
{
    use RefreshDatabase;

    public $statusCheckStructure = [
        'id',
        'rtt',
        'success',
        'check_error_id',
        'check_type_id',
        'created_at',
    ];
    public $checkTypeStructure = [
        'id',
        'name',
        'url',
        'description',
    ];
    public $checkErrorStructure = [
        'id',
        'exception_name',
        'message',
    ];
    public $indexStructure = [
        'data',
        'links',
        'meta',
    ];

    protected function setUp() : void
    {
        parent::setUp();
        CheckError::factory()->count(10)->create();
        CheckType::factory()->count(10)->create();
    }

    public function testStatusCheckIndex()
    {
        StatusCheck::factory()->count(5)->create();

        $response = $this->json('GET', '/status/statuschecks');

        $indexStructure = array_merge(
            $this->indexStructure,
            ['data' => [$this->statusCheckStructure]]
        );

        $response->assertOk()
            ->assertJsonStructure($indexStructure);
        $this->assertArrayNotHasKey('check_type', $response->json('data')[0], 'Expected check_type to not be in response');
        $this->assertArrayNotHasKey('check_error', $response->json('data')[0], 'Expected check_error to not be in response');
        $this->assertStringNotContainsString('expand', $response->json('links')['first'], 'Expected expand to not be in links');
    }

    public function testStatusCheckIndexExpandTypes()
    {
        StatusCheck::factory()->count(5)->create();

        $response = $this->json('GET', '/status/statuschecks?expand=check_type');

        $indexStructure = array_merge(
            $this->indexStructure,
            ['data' => [
                array_merge($this->statusCheckStructure,
                ['check_type' => $this->checkTypeStructure])
            ]]
        );

        $response->assertOk()
            ->assertJsonMissing(['check_error'])
            ->assertJsonStructure($indexStructure);
        $this->assertArrayNotHasKey('check_error', $response->json('data')[0], 'Expected check_error to not be in response');
        $this->assertStringContainsString('expand', $response->json('links')['first'], 'Expected expand to be in links');
    }

    public function testStatusCheckIndexExpandMultiple()
    {
        StatusCheck::factory()->count(5)->create();

        $response = $this->json('GET', '/status/statuschecks?expand=check_type,check_error');

        $indexStructure = array_merge(
            $this->indexStructure,
            ['data' => [
                array_merge($this->statusCheckStructure,
                //['check_error' => $this->checkErrorStructure], // COULD BE NULL
                ['check_type' => $this->checkTypeStructure])
            ]]
        );
        $response->assertOk()
            ->assertJsonStructure($indexStructure);
        $this->assertStringContainsString('expand', $response->json('links')['first'], 'Expected expand to be in links');
    }

    public function testCheckErrorIndex()
    {
        $response = $this->json('GET', '/status/checkerrors');

        $indexStructure = array_merge(
            $this->indexStructure,
            ['data' => [$this->checkErrorStructure]]
        );

        $response->assertOk()
            ->assertJsonStructure($indexStructure);
    }

    public function testCheckTypeIndex()
    {
        $response = $this->json('GET', '/status/checktypes');

        $indexStructure = array_merge(
            $this->indexStructure,
            ['data' => [$this->checkTypeStructure]]
        );

        $response->assertOk()
            ->assertJsonStructure($indexStructure);
    }

    public function testGetStatusCheck()
    {
        $sc = StatusCheck::factory()->create();
        $id = $sc->id;

        $response = $this->json('GET', "/status/statuschecks/${id}");

        $response->assertOk()
            ->assertJsonStructure(['data' => $this->statusCheckStructure]);
    }

    public function testGetCheckError()
    {
        $sc = CheckError::factory()->create();
        $id = $sc->id;

        $response = $this->json('GET', "/status/checkerrors/${id}");

        $response->assertOk()
            ->assertJsonStructure(['data' => $this->checkErrorStructure]);
    }

    public function testGetCheckType()
    {
        $sc = CheckType::factory()->create();
        $id = $sc->id;

        $response = $this->json('GET', "/status/checktypes/${id}");

        $response->assertOk()
            ->assertJsonStructure(['data' => $this->checkTypeStructure]);
    }

    public function testFailGetStatusCheck()
    {
        $response = $this->json('GET', "/status/statuschecks/999");

        $response->assertNotFound();
    }

    public function testFailGetCheckError()
    {
        $response = $this->json('GET', "/status/checkerrors/999");

        $response->assertNotFound();
    }

    public function testFailGetCheckType()
    {
        $response = $this->json('GET', "/status/checktypes/999");

        $response->assertNotFound();
    }
}
