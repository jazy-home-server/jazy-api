import { mount } from '@vue/test-utils';
import expect from 'expect';
import ExampleComponent from '@components/ExampleComponent.vue';

test('should match snapshot', () => {
    const wrapper = mount(ExampleComponent);

    expect(wrapper).toMatchSnapshot();
});
