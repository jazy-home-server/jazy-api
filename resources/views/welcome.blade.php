@extends('layouts.app')

@section('content')
<div class="flex-center position-ref full-height">
    <div>
        <div class="title m-b-md">
            {{ config('app.name') }}
        </div>
        <div class="">
            This is an API service for all of Jazy's applications. Made using Laravel.
        </div>
        @if (empty($databaseConnectionOk))
            <div class="warn">
                Database connection <strong class="">has failed</strong>
            </div>
        @else
            <div class="success">
                Database connection <strong class="">is ok</strong>
            </div>
        @endif
        <div class="small">
            EUID: {{ $euid }}
        </div>
    </div>
</div>
@endsection
