
@extends('layouts.app')

@section('styles')
.container {
    margin: 1rem;
    display: flex;
    flex-wrap: wrap;
}
.container__item {
    border: 2px white solid;
    margin: 1rem;
}
.error {
    flex-grow: 1;
    text-align: left;
}
.error__header {
    font-size: 1.3em;
    font-weight: bold;
    display: inline-block;
    padding: 0.2rem;
    border-right: 2px solid white;
    border-bottom: 2px solid white;
}
.error__property {
    padding: 1rem;
    border-bottom: solid 1px white;
}
.error__property:last-child {
    border-bottom: none;
}
.error__property__name {
    font-size: 0.9em;
    color: #faa;
    margin-left: 1rem;
}
.error__property__name::after {
    content: '- ';
}
.json {
}
.json__pair {
    margin: 5px;
    padding: 2px;
    border: 1px solid green;
}
.json__key {
    cursor: pointer;
    font-size: 0.8em;
    margin-left: 2rem;
    color: #4a4;
}
.json__key::after {
    content: ': ';
}
.json__value {
    white-space: pre;
}
.json__value--collapsed {
    display: none;
}
@endsection

@section('content')
<h1>Logged Errors</h1>
{{ $errors->links() }}
<div class="container">
    @foreach ($errors as $error)
    <div class="error container__item">
        <div class="error__header">
            ID: {{ $error->id }}
        </div>
        <div class="error__property json">
            <div class="error__property__name">
                Error Object
            </div>
            <div class="error__property__value">
                @foreach (json_decode($error->error, true) as $errorKey => $errorValue)
                <div class="json__pair">
                    <div class="json__key json__collapse-toggle">{{ $errorKey }}</div>
                    <div class="json__value json__value--collapsed">{{ $errorValue }}</div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="error__property">
            <div class="error__property__name">
                Extra Object
            </div>
            <div class="error__property__value">
                @foreach (json_decode($error->extra, true) as $errorKey => $errorValue)
                <div class="json__pair">
                    <div class="json__key json__collapse-toggle">{{ $errorKey }}</div>
                    <div class="json__value json__value--collapsed">{{ $errorValue }}</div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="error__property">
            <div class="error__property__name">
                Origin
            </div>
            <div class="error__property__value">
                {{ $error->origin }}
            </div>
        </div>
        <div class="error__property">
            <div class="error__property__name">
                IP
            </div>
            <div class="error__property__value">
                {{ $error->ip }}
            </div>
        </div>
        <div class="error__property">
            <div class="error__property__name">
                App Name
            </div>
            <div class="error__property__value">
                {{ $error->app }}
            </div>
        </div>
        <div class="error__property">
            <div class="error__property__name">
                Error Date
            </div>
            <div class="error__property__value">
                {{ $error->error_date }}
            </div>
        </div>
        <div class="error__property">
            <div class="error__property__name">
                API Logged Date
            </div>
            <div class="error__property__value">
                {{ $error->created_at }}
            </div>
        </div>
    </div>
    @endforeach
</div>
{{ $errors->links() }}
@endsection

@section('scripts')
<script type="text/javascript" src="{{ asset('js/meta/errorsview.js') }}"></script>
@endsection
