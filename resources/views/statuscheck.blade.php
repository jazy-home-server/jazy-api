@extends('layouts.app')

@section('styles')
.types-container {
    display: flex;
    flex-direction: column;
}
.type-container {
    display: flex;
    flex-direction: row;
}
.type-container--flexstart {
    min-width: 25em;
    width: 25em;
    overflow-y: auto;
}
.type-container--header {
    font-size: 1.3em;
    font-weight: bold;
}
.type-container--header > a {
    text-decoration: none;
    color: inherit;
}
.type-container--subheader {
    font-size: 0.8em;
}
.type-container--statuscheck {
    font-size: 0.7em;
}
.statuscheck-container {
    display: flex;
    flex-direction: row;
}
.statuscheck {
    display: flex;
    flex-direction: column;
    justify-content: center;
    border-left: white 1px solid;
    width: 6em;
    margin-left: 10px;
    padding-left: 10px;
}
.statuscheck--item {
}
.type-disabled {
    opacity: 0.6;
}
@endsection

@section('content')

    <div class="title">
        Service Statuses
    </div>
    <div class="">
        @if (empty($lastUpdate))
        N/A
        @else
        Checked at: {{ $lastUpdate->setTimezone($frontendTimezone)->format('c') }}
        @endif
    </div>
    <div class="m-b-md">
        Time to render: {{ (microtime(true) - $laravelStart) }} seconds
    </div>
    <div class="types-container m-b-md">
        @if (empty($types))
        <h2>No data avaialable</h2>
        @else
        @foreach ($types as $type)
        @if (!empty($type->StatusCheck) && !empty($type->StatusCheck[0]))
        <div class="type-container m-b-md">
            <div class="type-container--flexstart">
                <div class="type-container--header {{ $type->StatusCheck[0]->success ? 'success' : 'warn' }}">
                    <a href="{{ $type->url }}" target="_blank">{{ $type->url }}</a>
                </div>
                <div class="type-container--subheader">{{ $type->description }}</div>
            </div>
            <div class="type-container--statuscheck statuscheck-container">
            @foreach ($type->StatusCheck as $check)
                <div class="statuscheck">
                    <div
                        class="statuscheck--item {{ $check->success ? 'success' : 'warn' }}"
                        title="{{ $check->CheckError ? ($check->CheckError->exception_name . ' | ' . $check->CheckError->message) : 'No Error' }}"
                    >
                        {{ $check->success ? "Success" : "Fail" }}
                    </div>
                    <div class="statuscheck--item"><strong>{{ $check->rtt }}</strong></div>
                    <div class="statuscheck--item">
                        <div class="datetime datetime__date">{{ $check->created_at->setTimezone($frontendTimezone)->format('n/j/Y') }}</div>
                        <div class="datetime datetime__time" title="{{ $check->created_at->setTimezone($frontendTimezone)->format('T') }}">
                            {{ $check->created_at->setTimezone($frontendTimezone)->format('g:i A') }}
                        </div>
                    </div>
                </div>
            @endforeach
            @if (!empty($type->lastSuccess))
                <div class="statuscheck">
                    <div
                        class="caution"
                    >
                        Last Success
                    </div>
                    <div class="statuscheck--item">{{ $type->lastSuccess->rtt }}</div>
                    <div class="statuscheck--item">
                        <div class="datetime datetime__date"><strong>{{ $type->lastSuccess->created_at->setTimezone($frontendTimezone)->format('n/j/Y') }}</strong></div>
                        <div class="datetime datetime__time" title="{{ $type->lastSuccess->created_at->setTimezone($frontendTimezone)->format('T') }}">
                            {{ $type->lastSuccess->created_at->setTimezone($frontendTimezone)->format('g:i A') }}
                        </div>
                    </div>
                </div>
            @endif
            @if (!empty($type->lastFail))
                <div class="statuscheck">
                    <div
                        class="caution"
                        title="{{ $type->lastFail->CheckError ? ($type->lastFail->CheckError->exception_name . ' | ' . $type->lastFail->CheckError->message) : 'No Error' }}"
                    >
                        Last Fail
                    </div>
                    <div class="statuscheck--item">{{ $type->lastFail->rtt }}</div>
                    <div class="statuscheck--item">
                        <div class="datetime datetime__date"><strong>{{ $type->lastFail->created_at->setTimezone($frontendTimezone)->format('n/j/Y') }}</strong></div>
                        <div class="datetime datetime__time" title="{{ $type->lastFail->created_at->setTimezone($frontendTimezone)->format('T') }}">
                            {{ $type->lastFail->created_at->setTimezone($frontendTimezone)->format('g:i A') }}
                        </div>
                    </div>
                </div>
            @endif
            </div>
        </div>
        @endif
        @endforeach
        @endif
    </div>
@endsection
