function jsoncollapse(e) {
    const jsonValue = e.currentTarget.parentElement.querySelector('.json__value');
    if (jsonValue.classList.contains('json__value--collapsed'))
        jsonValue.classList.remove('json__value--collapsed');
    else
        jsonValue.classList.add('json__value--collapsed');
}

window.onload = () => {
    document.querySelectorAll('.json__collapse-toggle').forEach(element => {
        element.addEventListener('click', jsoncollapse);
    });
};
